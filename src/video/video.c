#include "src\misc\definer.h"
/*Set video mode*/
void set_mode(word mode) {
  union REGS regs;
  if(mode == VGA_256_COLOR_MODE) {
  	chunkymode = 1;
  } else {
  	chunkymode = 0;
  }
  regs.h.ah = SET_MODE + (byte)(mode/256);
  regs.h.al = mode&255;
  int386(VIDEO_INT, &regs, &regs);
}
/*turn on chain mode*/
void chainMode() {
  set_mode(VGA_256_COLOR_MODE);  
	outp(SC_INDEX, MEMORY_MODE);
	outp(SC_DATA, 0x06);
	outp(CRTC_INDEX, UNDERLINE_LOCATION);
	outp(CRTC_DATA, 0x00);
	outp(CRTC_INDEX, MODE_CONTROL);
	outp(CRTC_DATA, 0xe3);
	chainmode = 1;
  chunkymode = 0;
}
/*Set plane(s), use only in chainmode*/
void setPlane( byte planes) {
  outp(0x3c4, 0x02);
  outp(0x3c5, planes);
}