#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dos.h>
#include <math.h>
#include <string.h>


#define PACKED __attribute__((packed))
#pragma pack(1) 

void set_palette(unsigned int entry, unsigned char r, unsigned char g, unsigned char b) {
   outp(0x3c8, entry);
   outp(0x3c9, r);
   outp(0x3c9, g);
   outp(0x3c9, b);
}

void sm(unsigned int mode) {
  union REGS r;
  r.x.eax=0x4f02;
  r.x.ebx=mode;
  int386(0x10,&r,&r);
}
void setWindow(unsigned int window) {
  union REGS r;
  r.x.eax=0x4f05;
  r.x.ebx=0x0000;
  r.x.edx=window;
  int386(0x10,&r,&r);
}



