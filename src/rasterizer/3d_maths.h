#include <math.h>
#define x 0
#define y 1
#define z 2
#define w 3

void v_clear( float *v) ; //empty vector
void v_unit(float *result, float *v); //unit vector
void v_set(float *v, float *o); //set vector
void v_setf(float *v, float o); //set vector
void v_add(float *result, float *v, float *o); //add vector
void v_addf(float *result, float *v, float o); //sub vector
void v_sub(float *result, float *v, float *o); //sub vector
void v_subf(float *result, float *v, float o); //sub vector
void v_mul(float *result, float *v, float *o); //mul vector
void v_mulf(float *result, float *v, float o); //mul vector
void v_div(float *result, float *v, float *o); //div vector
void v_divf(float *result, float *v, float o); //div vector
float v_dot(float *v, float *o); //dot vector
void v_abs(float *result, float *v); //abs vector
float v_dist(float *v, float *o); //dist of vectors
void v_cross(float *result, float *v, float *o); //cross vector


void m_clear(float (*result)[4][4]) ;  //empty matrix4
void m_set(float (*result)[4][4], float (*v)[4][4]) ; //set matrix4
void m_setf(float (*result)[4][4], float o) ; //set matrix4
void m_add(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) ; //add matrix4
void m_addf(float (*result)[4][4], float (*v)[4][4], float o) ; //sub matrix4
void m_sub(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) ; //sub matrix4
void m_subf(float (*result)[4][4], float (*v)[4][4], float o) ; //sub matrix4
void m_mul(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) ; //mul matrix4
void m_mulf(float (*result)[4][4], float (*v)[4][4], float o) ; //mul matrix4
void m_div(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) ; //div matrix4
void m_divf(float (*result)[4][4], float (*v)[4][4], float o) ; //div matrix4
void m_abs(float (*result)[4][4], float (*v)[4][4]) ; //abs matrix4
void m_v_multiplication( float *result, float (*m)[4][4], float *v)  ; //abs matrix4