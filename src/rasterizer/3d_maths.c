#define x 0
#define y 1
#define z 2
#define w 3

void v_clear( float *v) { //empty vector
	v[x] = 0.0f;
	v[y] = 0.0f;
	v[z] = 0.0f;
	v[w] = 0.0f;
}
void v_unit( float *result, float *v) { //unit vector
	float l = sqrt( v[x]*v[x] + v[y]*v[y] + v[z]*v[z] + v[w]*v[w]);
	result[x] = v[x] / l;
	result[y] = v[y] / l;
	result[z] = v[z] / l;
	result[w] = v[w] / l;
}
void v_set( float *v, float *o) { //set vector
	v[x] = o[x];
	v[y] = o[y];
	v[z] = o[z];
	v[w] = o[w];
}
void v_setf( float *v, float o) { //set vector
	v[x] = o;
	v[y] = o;
	v[z] = o;
	v[w] = o;
}
void v_add( float *result, float *v, float *o) { //add vector
	result[x] = v[x] + o[x];
	result[y] = v[y] + o[y];
	result[z] = v[z] + o[z];
	result[w] = v[w] + o[w];
}
void v_addf( float *result, float *v, float o) { //sub vector
	result[x] = v[x] + o;
	result[y] = v[y] + o;
	result[z] = v[z] + o;
	result[w] = v[w] + o;
}
void v_sub( float *result, float *v, float *o) { //sub vector
	result[x] = v[x] - o[x];
	result[y] = v[y] - o[y];
	result[z] = v[z] - o[z];
	result[w] = v[w] - o[w];
}
void v_subf( float *result, float *v, float o) { //sub vector
	result[x] = v[x] - o;
	result[y] = v[y] - o;
	result[z] = v[z] - o;
	result[w] = v[w] - o;
}
void v_mul( float *result, float *v, float *o) { //mul vector
	result[x] = v[x] * o[x];
	result[y] = v[y] * o[y];
	result[z] = v[z] * o[z];
	result[w] = v[w] * o[w];
}
void v_mulf( float *result, float *v, float o) { //mul vector
	result[x] = v[x] * o;
	result[y] = v[y] * o;
	result[z] = v[z] * o;
	result[w] = v[w] * o;
}
void v_div( float *result, float *v, float *o) { //div vector
	result[x] = v[x] / o[x];
	result[y] = v[y] / o[y];
	result[z] = v[z] / o[z];
	result[w] = v[w] / o[w];
}
void v_divf( float *result, float *v, float o) { //div vector
	result[x] = v[x] / o;
	result[y] = v[y] / o;
	result[z] = v[z] / o;
	result[w] = v[w] / o;
}
float v_dot( float *v, float *o) { //dot vector
	return v[x] * o[x] + v[y] * o[y] + v[z] * o[z] + v[w] * o[w];
}
void v_abs( float *result, float *v) { //abs vector
	result[x] = abs(v[x]);
	result[y] = abs(v[y]);
	result[z] = abs(v[z]);
	result[w] = abs(v[w]);
}
float v_dist( float *v, float *o) { //dist of vectors
	float X = v[x] - o[x];
	float Y = v[y] - o[y];
	float Z = v[z] - o[z];
	float W = v[w] - o[w];
	return sqrt( X*X + Y*Y + Z*Z + W*W);
}
void v_cross( float *result, float *v, float *o) { //cross vector
    result[x] = v[y]*o[z] - v[z]*o[y];
    result[y] = v[z]*o[x] - v[x]*o[z];
    result[z] = v[x]*o[y] - v[y]*o[x];
}
void v_swap( float *v, float *o) { //cross vector
	float temp[3];
	temp[0] = o[0];
	temp[1] = o[1];
	temp[2] = o[2];
	o[0] = v[0];
	o[1] = v[1];
	o[2] = v[2];
	v[0] = temp[0];
	v[1] = temp[1];
	v[2] = temp[2];
}





void m_clear(float (*v)[4][4]) {  //empty matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*v)[Y][X] = 0;
}
void m_set(float (*v)[4][4], float (*o)[4][4]) { //set matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*v)[Y][X] = (*o)[Y][X];
}
void m_setf(float (*v)[4][4], float o) { //set matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*v)[Y][X] = o;
}
void m_add(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) { //add matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] + (*o)[Y][X];
}
void m_addf(float (*result)[4][4], float (*v)[4][4], float o) { //sub matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] + 0;
}
void m_sub(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) { //sub matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] - (*o)[Y][X];
}
void m_subf(float (*result)[4][4], float (*v)[4][4], float o) { //sub matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] - o;
}
void m_mul(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) { //mul matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] * (*o)[Y][X];
}
void m_mulf(float (*result)[4][4], float (*v)[4][4], float o) { //mul matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] * o;
}
void m_div(float (*result)[4][4], float (*v)[4][4], float (*o)[4][4]) { //div matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] / (*o)[Y][X];
}
void m_divf(float (*result)[4][4], float (*v)[4][4], float o) { //div matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = (*v)[Y][X] / o;
}
void m_abs(float (*result)[4][4], float (*v)[4][4]) { //abs matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			(*result)[Y][X] = abs((*v)[Y][X]);
}
void m_v_multiplication( float *result, float (*m)[4][4], float *v) { //abs matrix4
	int X,Y;
	for(X=0; X<4; X++)
		for(Y=0; Y<4; Y++)
			result[X] += (*m)[Y][X] * v[y];    
}