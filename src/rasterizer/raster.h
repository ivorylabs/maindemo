/*Change transform space*/
void transform(float *object_axis, float *camerapos, float *camera_axis);

/*Pushes Triangle to be drawn later*/
void pushTriangle(float *p0, float *p1, float *p2);

/*Draw Triangle*/
void drawTriangle(float *p0, float *p1, float *p2);

void plotpixel(int _x, int _y, unsigned long c);

void drawLine(float x0, float x1, float Y);

void draw3D();

void line_fast(int x1, int y1, int x2, int y2);



void setFilled(int _filled);
void setLined(int _lined);


void drawTriangle_noalpha(float *p1, float *p2, float *p3);