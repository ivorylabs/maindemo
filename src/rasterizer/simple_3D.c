#include "src\rasterizer\raster.h"
#include "src\rasterizer\simple_3D.h"
#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <conio.h>
#include <mem.h>
#include <math.h>
float vert2D[1500][4];

extern int filled;
extern int lined;

int vertpointer = 0;
void reset_raster() {
	vertpointer = 0;
}
void poly_transformer( float *p1, float *p2, float *p3, float *pos_obj, float *rot_obj, float *pos_cam, float *rot_cam) {

	float d1[3], d2[3], d3[3];

	float cx = cos( rot_obj[ 0 ] );
	float cy = cos( rot_obj[ 1 ] );
	float cz = cos( rot_obj[ 2 ] );
	float sx = sin( rot_obj[ 0 ] );
	float sy = sin( rot_obj[ 1 ] );
	float sz = sin( rot_obj[ 2 ] );

	//reworked


	d1[0] = p1[0] * cx - p1[2] * sx;
	d1[2] = p1[0] * sx + p1[2] * cx;
	p1[0] = d1[0];
	p1[2] = d1[2];
	d2[0] = p2[0] * cx - p2[2] * sx;
	d2[2] = p2[0] * sx + p2[2] * cx;
	p2[0] = d2[0];
	p2[2] = d2[2];
	d3[0] = p3[0] * cx - p3[2] * sx;
	d3[2] = p3[0] * sx + p3[2] * cx;
	p3[0] = d3[0];
	p3[2] = d3[2];

	d1[1] = p1[1] * cy - p1[2] * sy;
	d1[2] = p1[1] * sy + p1[2] * cy;
	p1[1] = d1[1];
	p1[2] = d1[2];
	d2[1] = p2[1] * cy - p2[2] * sy;
	d2[2] = p2[1] * sy + p2[2] * cy;
	p2[1] = d2[1];
	p2[2] = d2[2];
	d3[1] = p3[1] * cy - p3[2] * sy;
	d3[2] = p3[1] * sy + p3[2] * cy;
	p3[1] = d3[1];
	p3[2] = d3[2];

	d1[0] = p1[0] * cz - p1[1] * sz;
	d1[1] = p1[0] * sz + p1[1] * cz;
	p1[0] = d1[0];
	p1[1] = d1[1];
	d2[0] = p2[0] * cz - p2[1] * sz;
	d2[1] = p2[0] * sz + p2[1] * cz;
	p2[0] = d2[0];
	p2[1] = d2[1];
	d3[0] = p3[0] * cz - p3[1] * sz;
	d3[1] = p3[0] * sz + p3[1] * cz;
	p3[0] = d3[0];
	p3[1] = d3[1];

	p1[0] = d1[0] + pos_obj[0];
	p1[1] = d1[1] + pos_obj[1];
	p1[2] = d1[2] + pos_obj[2];
	p2[0] = d2[0] + pos_obj[0];
	p2[1] = d2[1] + pos_obj[1];
	p2[2] = d2[2] + pos_obj[2];
	p3[0] = d3[0] + pos_obj[0];
	p3[1] = d3[1] + pos_obj[1];
	p3[2] = d3[2] + pos_obj[2];




	cx = cos( rot_cam[ 0 ] );
	cy = cos( rot_cam[ 1 ] );
	cz = cos( rot_cam[ 2 ] );
	sx = sin( rot_cam[ 0 ] );
	sy = sin( rot_cam[ 1 ] );
	sz = sin( rot_cam[ 2 ] );

	//reworked
	
	d1[0] = p1[0] * cx - p1[2] * sx;
	d1[2] = p1[0] * sx + p1[2] * cx;
	p1[0] = d1[0];
	p1[2] = d1[2];
	d2[0] = p2[0] * cx - p2[2] * sx;
	d2[2] = p2[0] * sx + p2[2] * cx;
	p2[0] = d2[0];
	p2[2] = d2[2];
	d3[0] = p3[0] * cx - p3[2] * sx;
	d3[2] = p3[0] * sx + p3[2] * cx;
	p3[0] = d3[0];
	p3[2] = d3[2];

	d1[1] = p1[1] * cy - p1[2] * sy;
	d1[2] = p1[1] * sy + p1[2] * cy;
	p1[1] = d1[1];
	p1[2] = d1[2];
	d2[1] = p2[1] * cy - p2[2] * sy;
	d2[2] = p2[1] * sy + p2[2] * cy;
	p2[1] = d2[1];
	p2[2] = d2[2];
	d3[1] = p3[1] * cy - p3[2] * sy;
	d3[2] = p3[1] * sy + p3[2] * cy;
	p3[1] = d3[1];
	p3[2] = d3[2];

	d1[0] = p1[0] * cz - p1[1] * sz;
	d1[1] = p1[0] * sz + p1[1] * cz;
	p1[0] = d1[0];
	p1[1] = d1[1];
	d2[0] = p2[0] * cz - p2[1] * sz;
	d2[1] = p2[0] * sz + p2[1] * cz;
	p2[0] = d2[0];
	p2[1] = d2[1];
	d3[0] = p3[0] * cz - p3[1] * sz;
	d3[1] = p3[0] * sz + p3[1] * cz;
	p3[0] = d3[0];
	p3[1] = d3[1];

	p1[0] = d1[0] + pos_cam[0];
	p1[1] = d1[1] + pos_cam[1];
	p1[2] = d1[2] + pos_cam[2];
	p2[0] = d2[0] + pos_cam[0];
	p2[1] = d2[1] + pos_cam[1];
	p2[2] = d2[2] + pos_cam[2];
	p3[0] = d3[0] + pos_cam[0];
	p3[1] = d3[1] + pos_cam[1];
	p3[2] = d3[2] + pos_cam[2];

}
int comp(const void* a, const void* b) {
	float** vertex1 = (float*)a;
	float** vertex2 = (float*)b;
	if (min(min(vertex1[0][2],vertex1[1][2]),vertex1[2][2]) > min(min(vertex2[0][2],vertex2[1][2]),vertex2[2][2]) ) return -1;
	if (min(min(vertex1[0][2],vertex1[1][2]),vertex1[2][2]) == min(min(vertex2[0][2],vertex2[1][2]),vertex2[2][2]) ) return 0;
	if (min(min(vertex1[0][2],vertex1[1][2]),vertex1[2][2]) < min(min(vertex2[0][2],vertex2[1][2]),vertex2[2][2]) ) return 1;
}
void end3D(float *pos_obj, float *rot_obj, float *pos_cam, float *rot_cam) {
	int i;
	for(i=0; i<vertpointer; i+=3) {
		poly_transformer( &(vert2D[i+0]), &(vert2D[i+1]), &(vert2D[i+2]), pos_obj, rot_obj, pos_cam, rot_cam);
	}
	//if(filled==1)
	//	sortpolys();
	//qsort(vert2D, vertpointer/12, sizeof(float)*12, comp);
	for(i=0; i<vertpointer; i+=3) {
		make3Dto2Dscreensized( &(vert2D[i+0]), &(vert2D[i+1]), &(vert2D[i+2]));
	}

}
void sortedend3D(float *pos_obj, float *rot_obj, float *pos_cam, float *rot_cam) {
	int i;
	for(i=0; i<vertpointer; i+=3) {
		poly_transformer( &(vert2D[i+0]), &(vert2D[i+1]), &(vert2D[i+2]), pos_obj, rot_obj, pos_cam, rot_cam);
	}
	if(filled==1)
		sortpolys();
	//qsort(vert2D, vertpointer/12, sizeof(float)*12, comp);
	for(i=0; i<vertpointer; i+=3) {
		make3Dto2Dscreensized( &(vert2D[i+0]), &(vert2D[i+1]), &(vert2D[i+2]));
	}

}
void pushPolygon(float *p1, float *p2, float *p3, unsigned char r, unsigned char g, unsigned char b) {
	float r1[3], r2[3], r3[3];
	vert2D[vertpointer][0] = p1[0];
	vert2D[vertpointer][1] = p1[1];
	vert2D[vertpointer][2] = p1[2];
	vert2D[vertpointer][3] = r;
	vertpointer++;
	vert2D[vertpointer][0] = p2[0];
	vert2D[vertpointer][1] = p2[1];
	vert2D[vertpointer][2] = p2[2];
	vert2D[vertpointer][3] = g;
	vertpointer++;
	vert2D[vertpointer][0] = p3[0];
	vert2D[vertpointer][1] = p3[1];
	vert2D[vertpointer][2] = p3[2];
	vert2D[vertpointer][3] = b;
	vertpointer++;
}
void make3Dto2Dscreensized(float *p1, float *p2, float *p3) {
	float r1[3], r2[3], r3[3];
	float fov = 120.0f;
	r1[0] = p1[0] / (p1[2] * fov/180.0f + 1.0f);
	r1[1] = p1[1] / (p1[2] * fov/180.0f + 1.0f);
	r2[0] = p2[0] / (p2[2] * fov/180.0f + 1.0f);
	r2[1] = p2[1] / (p2[2] * fov/180.0f + 1.0f);
	r3[0] = p3[0] / (p3[2] * fov/180.0f + 1.0f);
	r3[1] = p3[1] / (p3[2] * fov/180.0f + 1.0f);

	r1[0] *= 160.0f;
	r1[1] *= 160.0f;
	r2[0] *= 160.0f;
	r2[1] *= 160.0f;
	r3[0] *= 160.0f;
	r3[1] *= 160.0f;

	r1[0] += 160.0f;
	r1[1] += 100.0f;
	r2[0] += 160.0f;
	r2[1] += 100.0f;
	r3[0] += 160.0f;
	r3[1] += 100.0f;


	p1[0] = r1[0];
	p1[1] = r1[1];
	p2[0] = r2[0];
	p2[1] = r2[1];
	p3[0] = r3[0];
	p3[1] = r3[1];

}
void swap(int c, int d) {
  float swap[4];
      	swap[0] = vert2D[d][0];
      	swap[1] = vert2D[d][1];
      	swap[2] = vert2D[d][2];
      	swap[3] = vert2D[d][3];
      	vert2D[d][0] = vert2D[c][0];
      	vert2D[d][1] = vert2D[c][1];
      	vert2D[d][2] = vert2D[c][2];
      	vert2D[d][3] = vert2D[c][3];
      	vert2D[c][0] = swap[0];
      	vert2D[c][1] = swap[1];
      	vert2D[c][2] = swap[2];
      	vert2D[c][3] = swap[3];


      	swap[0] = vert2D[d+1][0];
      	swap[1] = vert2D[d+1][1];
      	swap[2] = vert2D[d+1][2];
      	swap[3] = vert2D[d+1][3];
      	vert2D[d+1][0] = vert2D[c+1][0];
      	vert2D[d+1][1] = vert2D[c+1][1];
      	vert2D[d+1][2] = vert2D[c+1][2];
      	vert2D[d+1][3] = vert2D[c+1][3];
      	vert2D[c+1][0] = swap[0];
      	vert2D[c+1][1] = swap[1];
      	vert2D[c+1][2] = swap[2];
      	vert2D[c+1][3] = swap[3];

      	swap[0] = vert2D[d+2][0];
      	swap[1] = vert2D[d+2][1];
      	swap[2] = vert2D[d+2][2];
      	swap[3] = vert2D[d+2][3];
      	vert2D[d+2][0] = vert2D[c+2][0];
      	vert2D[d+2][1] = vert2D[c+2][1];
      	vert2D[d+2][2] = vert2D[c+2][2];
      	vert2D[d+2][3] = vert2D[c+2][3];
      	vert2D[c+2][0] = swap[0];
      	vert2D[c+2][1] = swap[1];
      	vert2D[c+2][2] = swap[2];
      	vert2D[c+2][3] = swap[3];
}
int sorted() {
	int d;
	for (d = 0 ; d < vertpointer-3; d+=3)
	{
		if ( min(min(vert2D[d+0][2],vert2D[d+1][2]),vert2D[d+2][2]) > min(min(vert2D[d+3][2],vert2D[d+4][2]),vert2D[d+5][2]) )
			return 0;
	}
	return 1;
}
void sortpolys() {
  int c,d;
  int swaps = 0;
  for (c = 0 ; c < vertpointer-3; c+=3)
  {
  	int smallest_indx = c;
    if ( min(min(vert2D[c+0][2],vert2D[c+1][2]),vert2D[c+2][2]) > min(min(vert2D[c+3][2],vert2D[c+4][2]),vert2D[c+5][2]) )  /* For decreasing order use < */
    {
     swap( d, d+3);
    }
  }
  for (c = 0 ; c < vertpointer-3; c+=3)
  {
  	int smallest_indx = c;
    for (d = c ; d < vertpointer; d+=6)
    {
      if((swaps%1000)==0)
      	if(sorted())
      		return;
      if ( min(min(vert2D[d+0][2],vert2D[d+1][2]),vert2D[d+2][2]) > min(min(vert2D[c+0][2],vert2D[c+1][2]),vert2D[c+2][2]) )  /* For decreasing order use < */
      {
      	swap( c, d);
      	swaps++;
      }
    }
  }
}
void rasterize() {
	int i;
	for(i=0; i<vertpointer; i+=3) {
		if(vert2D[i+0][2]>0.1f && vert2D[i+1][2]>0.1f && vert2D[i+2][2]>0.1f)
        	drawTriangle(&(vert2D[i+0]), &(vert2D[i+1]), &(vert2D[i+2]));
	}
}
void noalpha_rasterize() {
	int i;
	for(i=0; i<vertpointer; i+=3) {
		if(vert2D[i+0][2]>0.1f && vert2D[i+1][2]>0.1f && vert2D[i+2][2]>0.1f)
        	drawTriangle_noalpha(&(vert2D[i+0]), &(vert2D[i+1]), &(vert2D[i+2]));
	}
}