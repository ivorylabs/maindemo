#include "3d_maths.h"
#include "src\misc\muual.h"

int datapointer = 0;
float vectors[1000];
float uv[1000];
unsigned long textures[8][32][32];
extern byte videobuffer[320*400*3];
dword *videobuffer32=(dword *)&videobuffer; 
int filled = 1;
int lined = 1;
void setFilled(int _filled) {
	filled = _filled;
}
void setLined(int _lined) {
	lined = _lined;
}
#define sgn(x) ((x<0)?-1:((x>0)?1:0)) /* macro to return the sign of a
                                         number */

void draw3D() {
	int i;
	for(i=0; i<datapointer; i+=9) {
		float p1[3];
		float t1[3];
		float p2[3];
		float t2[3];
		float p3[3];
		float t3[3];
		p1[0] = vectors[i+0];
		p1[1] = vectors[i+1];
		p1[2] = vectors[i+2];
		p2[0] = vectors[i+3];
		p2[1] = vectors[i+4];
		p2[2] = vectors[i+5];
		p3[0] = vectors[i+6];
		p3[1] = vectors[i+7];
		p3[2] = vectors[i+8];
		t1[0] = uv[i+0];
		t1[1] = uv[i+1];
		t1[2] = uv[i+2];
		t2[0] = uv[i+3];
		t2[1] = uv[i+4];
		t2[2] = uv[i+5];
		t3[0] = uv[i+6];
		t3[1] = uv[i+7];
		t3[2] = uv[i+8];
		drawTriangle(&p1, &p2, &p3);
	}
	datapointer = 0;
}
void plotpixel(int _x, int _y, unsigned long c) {
	if(_x<0 || _x>319 || _y<0 || _y>199) return;
	videobuffer32[_y*320+_x] = c;
}
void drawLine(float x0, float x1, float Y, unsigned long c) {
	int X;
	if(Y<0 || Y>199) return;
	if( (x0<0 && x1<0) || (x0>319 && x1>319)) return;
	if(x0 > x1) {
		int pointer = (int)Y*320+(int)x1;
		for(X = x1; X < x0; X+=2) {
			if(X>=0 && X<320) {
					videobuffer32[pointer] += c;
					videobuffer32[pointer+1] += c;
			}
			pointer+=2;
		}
	} else {
		int pointer = (int)Y*320+(int)x0;
		for(X = x0; X < x1; X++) {
			if(X>=0 && X<320)
					videobuffer32[pointer] += c;
			pointer++;
		}
	}
}
void fillBottomFlatTriangle(float *v1, float *v2, float *v3, unsigned char r, unsigned char g, unsigned char b)
{
  unsigned long c = 0x00000001 * b + 0x00010000 * r + 0x00000100 * g;
  float invslope1 = (v2[0] - v1[0]) / (v2[1] - v1[1]);
  float invslope2 = (v3[0] - v1[0]) / (v3[1] - v1[1]);

  float curx1 = v1[0];
  float curx2 = v1[0];
  float scanlineY;
  if(filled) {
	  for (scanlineY = ceil(v1[1]); scanlineY <= floor(v2[1]); scanlineY++)
	  {
	    drawLine(curx1, curx2, scanlineY, c);
	    curx1 += invslope1;
	    curx2 += invslope2;
	  }
  }
  if(lined) {
	  line_fast((int)v1[0], (int)v1[1], (int)v3[0], (int)v3[1], c);
	  line_fast((int)v1[0], (int)v1[1], (int)v2[0], (int)v2[1], c);
  }
}
void fillTopFlatTriangle(float *v1, float *v2, float *v3, unsigned char r, unsigned char g, unsigned char b)
{
  unsigned long c = 0x00000001 * b + 0x00010000 * r + 0x00000100 * g;
  
  float invslope1 = (v3[0] - v1[0]) / (v3[1] - v1[1]);
  float invslope2 = (v3[0] - v2[0]) / (v3[1] - v2[1]);

  float curx1 = v3[0];
  float curx2 = v3[0];
  float scanlineY;

  float tex_point1[4];
  float tex_step1[4];

  float tex_point2[4];
  float tex_step2[4];

  if(filled) {
	  for (scanlineY = floor(v3[1]); scanlineY > floor(v1[1]); scanlineY--)
	  {
	    drawLine(curx1, curx2, scanlineY, c);
	    curx1 -= invslope1;
	    curx2 -= invslope2;
	  }
  }
  if(lined) {
	  line_fast((int)v3[0], (int)v3[1], (int)v2[0], (int)v2[1], c);
	  line_fast((int)v3[0], (int)v3[1], (int)v1[0], (int)v1[1], c);
	}
}
void line_fast(int x1, int y1, int x2, int y2, unsigned long c)
{
  int i,dx,dy,sdx,sdy,dxabs,dyabs,X,Y,px,py;
  dx=x2-x1;      /* the horizontal distance of the line */
  dy=y2-y1;      /* the vertical distance of the line */
  dxabs=abs(dx);
  dyabs=abs(dy);
  sdx=sgn(dx);
  sdy=sgn(dy);
  X=dyabs>>1;
  Y=dxabs>>1;
  px=x1;
  py=y1;
  if(y1==y2 && (x1>310 || x2>310)) return;
  if(px<0 || px>319 || py<0 || py>199) { return; }
  if(px<319 && px>0) { 
  	videobuffer32[px+py*320] = c; 
  }

  if (dxabs>=dyabs) /* the line is more horizontal than vertical */
  {
    for(i=0;i<dxabs;i++)
    {
      Y+=dyabs;
      if (Y>=dxabs)
      {
        Y-=dxabs;
        py+=sdy;
      }
      px+=sdx;
	  if(px<0 || px>319 || py<0 || py>199) { break; }
  	  videobuffer32[px+py*320] = c;
    }
  }
  else /* the line is more vertical than horizontal */
  {
    for(i=0;i<dyabs;i++)
    {
      X+=dxabs;
      if (X>=dyabs)
      {
        X-=dyabs;
        px+=sdx;
      }
      py+=sdy;
	  if(px<0 || px>319 || py<0 || py>199) { break; }
	  videobuffer32[px+py*320] = c;
    }
  }
}
void drawTriangle(float *p1, float *p2, float *p3) {
	float dist1_1 = 0.0f;
    float p4[4];
    float tp3[2], tp2[2], tp1[2];
    if(p1[2]<0.8f || p2[2]<0.8f || p3[2]<0.8f ) return;
	if(p3[1]>p2[1] && p3[1]>p1[1]) {
		tp3[0] = p3[0];
		tp3[1] = p3[1];
		if(p2[1] > p1[1]) {
			tp2[0] = p2[0];
			tp2[1] = p2[1];
			tp1[0] = p1[0];
			tp1[1] = p1[1];
		} else {
			tp2[0] = p1[0];
			tp2[1] = p1[1];
			tp1[0] = p2[0];
			tp1[1] = p2[1];
		}
	} else if(p2[1]>p1[1] && p2[1]>p3[1]) {
		tp3[0] = p2[0];
		tp3[1] = p2[1];
		if(p3[1] > p1[1]) {
			tp2[0] = p3[0];
			tp2[1] = p3[1];
			tp1[0] = p1[0];
			tp1[1] = p1[1];
		} else {
			tp2[0] = p1[0];
			tp2[1] = p1[1];
			tp1[0] = p3[0];
			tp1[1] = p3[1];
		}
	} else {
		tp3[0] = p1[0];
		tp3[1] = p1[1];
		if(p3[1] > p2[1]) {
			tp2[0] = p3[0];
			tp2[1] = p3[1];
			tp1[0] = p2[0];
			tp1[1] = p2[1];
		} else {
			tp2[0] = p2[0];
			tp2[1] = p2[1];
			tp1[0] = p3[0];
			tp1[1] = p3[1];
		}
	}
	p1[0] = floor(tp1[0]);
	p1[1] = floor(tp1[1]);
	p2[0] = floor(tp2[0]);
	p2[1] = floor(tp2[1]);
	p3[0] = floor(tp3[0]);
	p3[1] = floor(tp3[1]);

	p4[0] = p1[0] + ( (p2[1] - p1[1]) / (p3[1] - p1[1]) ) * (p3[0] - p1[0]);
	p4[1] = p2[1];
	fillBottomFlatTriangle(p1, p2, p4, (unsigned char)p1[3], (unsigned char)p2[3], (unsigned char)p3[3]);
	fillTopFlatTriangle(p2, p4, p3, (unsigned char)p1[3], (unsigned char)p2[3], (unsigned char)p3[3]);
	
    
}



void noalpha_drawLine(float x0, float x1, float Y, unsigned long c) {
	int X;
	if(Y<0 || Y>199) return;
	if( (x0<0 && x1<0) || (x0>319 && x1>319)) return;
	if(x0 > x1) {
		int pointer = (int)Y*320+(int)x1;
		for(X = x1; X < x0; X+=2) {
			if(X>=0 && X<320) {
					videobuffer32[pointer] = c;
					videobuffer32[pointer+1] = c;
			}
			pointer+=2;
		}
	} else {
		int pointer = (int)Y*320+(int)x0;
		for(X = x0; X < x1; X++) {
			if(X>=0 && X<320)
					videobuffer32[pointer] = c;
			pointer++;
		}
	}
}
void noalpha_fillBottomFlatTriangle(float *v1, float *v2, float *v3, unsigned char r, unsigned char g, unsigned char b)
{
  unsigned long c = 0x00000001 * b + 0x00010000 * r + 0x00000100 * g;
  float invslope1 = (v2[0] - v1[0]) / (v2[1] - v1[1]);
  float invslope2 = (v3[0] - v1[0]) / (v3[1] - v1[1]);

  float curx1 = v1[0];
  float curx2 = v1[0];
  float scanlineY;
  if(filled) {
	  for (scanlineY = ceil(v1[1]); scanlineY <= floor(v2[1]); scanlineY++)
	  {
	    noalpha_drawLine(curx1, curx2, scanlineY, c);
	    curx1 += invslope1;
	    curx2 += invslope2;
	  }
  }
  if(lined) {
	  line_fast((int)v1[0], (int)v1[1], (int)v3[0], (int)v3[1], c);
	  line_fast((int)v1[0], (int)v1[1], (int)v2[0], (int)v2[1], c);
  }
}
void noalpha_fillTopFlatTriangle(float *v1, float *v2, float *v3, unsigned char r, unsigned char g, unsigned char b)
{
  unsigned long c = 0x00000001 * b + 0x00010000 * r + 0x00000100 * g;
  
  float invslope1 = (v3[0] - v1[0]) / (v3[1] - v1[1]);
  float invslope2 = (v3[0] - v2[0]) / (v3[1] - v2[1]);

  float curx1 = v3[0];
  float curx2 = v3[0];
  float scanlineY;

  float tex_point1[4];
  float tex_step1[4];

  float tex_point2[4];
  float tex_step2[4];

  if(filled) {
	  for (scanlineY = floor(v3[1]); scanlineY > floor(v1[1]); scanlineY--)
	  {
	    noalpha_drawLine(curx1, curx2, scanlineY, c);
	    curx1 -= invslope1;
	    curx2 -= invslope2;
	  }
  }
  if(lined) {
	  line_fast((int)v3[0], (int)v3[1], (int)v2[0], (int)v2[1], c);
	  line_fast((int)v3[0], (int)v3[1], (int)v1[0], (int)v1[1], c);
	}
}



void drawTriangle_noalpha(float *p1, float *p2, float *p3) {
	float dist1_1 = 0.0f;
    float p4[4];
    float tp3[2], tp2[2], tp1[2];
    if(p1[2]<0.8f || p2[2]<0.8f || p3[2]<0.8f ) return;
	if(p3[1]>p2[1] && p3[1]>p1[1]) {
		tp3[0] = p3[0];
		tp3[1] = p3[1];
		if(p2[1] > p1[1]) {
			tp2[0] = p2[0];
			tp2[1] = p2[1];
			tp1[0] = p1[0];
			tp1[1] = p1[1];
		} else {
			tp2[0] = p1[0];
			tp2[1] = p1[1];
			tp1[0] = p2[0];
			tp1[1] = p2[1];
		}
	} else if(p2[1]>p1[1] && p2[1]>p3[1]) {
		tp3[0] = p2[0];
		tp3[1] = p2[1];
		if(p3[1] > p1[1]) {
			tp2[0] = p3[0];
			tp2[1] = p3[1];
			tp1[0] = p1[0];
			tp1[1] = p1[1];
		} else {
			tp2[0] = p1[0];
			tp2[1] = p1[1];
			tp1[0] = p3[0];
			tp1[1] = p3[1];
		}
	} else {
		tp3[0] = p1[0];
		tp3[1] = p1[1];
		if(p3[1] > p2[1]) {
			tp2[0] = p3[0];
			tp2[1] = p3[1];
			tp1[0] = p2[0];
			tp1[1] = p2[1];
		} else {
			tp2[0] = p2[0];
			tp2[1] = p2[1];
			tp1[0] = p3[0];
			tp1[1] = p3[1];
		}
	}
	p1[0] = floor(tp1[0]);
	p1[1] = floor(tp1[1]);
	p2[0] = floor(tp2[0]);
	p2[1] = floor(tp2[1]);
	p3[0] = floor(tp3[0]);
	p3[1] = floor(tp3[1]);

	p4[0] = p1[0] + ( (p2[1] - p1[1]) / (p3[1] - p1[1]) ) * (p3[0] - p1[0]);
	p4[1] = p2[1];
	noalpha_fillBottomFlatTriangle(p1, p2, p4, (unsigned char)p1[3], (unsigned char)p2[3], (unsigned char)p3[3]);
	noalpha_fillTopFlatTriangle(p2, p4, p3, (unsigned char)p1[3], (unsigned char)p2[3], (unsigned char)p3[3]);
	
    
}