
void reset_raster();
void make3Dto2Dscreensized(float *p1, float *p2, float *p3);
void rasterize();
void sortpolys();
void end3D(float *pos_obj, float *rot_obj, float *pos_cam, float *rot_cam);
void pushPolygon(float *p1, float *p2, float *p3, unsigned char r, unsigned char g, unsigned char b);
int compare(const void* a, const void* b);
void noalpha_rasterize();
void sortedend3D(float *pos_obj, float *rot_obj, float *pos_cam, float *rot_cam);