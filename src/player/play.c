#include "src\misc\definer.h"
#include "src\misc\muual.h"
#include "src\player\magic.h"
#include "src\beeper\beeper.h"
#include "src\timing\timing.h"
#include "src\memory\buffer.h"
#include "src\misc\midasdll.h"


extern MIDASmodulePlayHandle playHandle;
extern MIDASmodule module;
/* this function is called immediately ***before*** Vertical Retrace starts */
void MIDAS_CALL PreVR(void){
	int i;
	frameCount++;
}
/* this function is called immediately ***after*** Vertical Retrace starts */
void MIDAS_CALL immVR(void){
}
/* this function is called some time later during Vertical Retrace */
void MIDAS_CALL inVR(void){

}
void endPlayer() {
    MIDASstopSample(playHandle);
    MIDASfreeSample(module);
    MIDASstopBackgroundPlay();
    MIDASclose();
}