#include "src\misc\definer.h"
#include "src\misc\muual.h"
#include "src\player\magic.h"
#include "src\beeper\beeper.h"
#include "src\timing\timing.h"
#include "src\memory\buffer.h"
#include "src\misc\midasdll.h"

extern byte videobuffer[320*200*3];
extern dword *videobuffer32=(dword *)&videobuffer; 
byte *VGA=(byte *)0xA0000; 
dword *VGA32=(dword *)0xA0000; 
void fill_rgbX8888() {
    int i,j;
    j = 0;
    setWindow(0);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = videobuffer32[j++];
    }
    setWindow(1);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = videobuffer32[j++];
    }
    setWindow(2);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = videobuffer32[j++];
    }
    setWindow(3);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = videobuffer32[j++];
    }
}
void fill_rgb888() {
    long i,j,window;
    j = 0;
    window=0;
    setWindow(window);
    while(j < 320*200*4) {
        VGA[i++] = videobuffer[j++];
        VGA[i++] = videobuffer[j++];
        VGA[i++] = videobuffer[j++];
        j++;
        if(i>=65536) {
            window++;
            setWindow(window);
            i=0;
        }
    }
}
/*  pushes video buffer into vga card memory if under regular chunky mode;
    in unchained mode moves video display memory reading address*/
void push_videobuffer() {
    if( videotype = RGBX8888 ) {
        fill_rgbX8888();
    } else if( videotype = RGB888 ) {
        fill_rgb888();
    } else {
        //uh-oh fake truecolor still not implemented
        int i=0;
    }
}

extern byte spinvideobuffer[320*200*3];
extern dword *spinvideobuffer32=(dword *)&spinvideobuffer; 
void spinfill_rgbX8888() {
    int i,j;
    j = 0;
    setWindow(0);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = spinvideobuffer32[j++];
    }
    setWindow(1);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = spinvideobuffer32[j++];
    }
    setWindow(2);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = spinvideobuffer32[j++];
    }
    setWindow(3);
    for(i = 0; i < 65536/4; i++) {
        VGA32[i] = spinvideobuffer32[j++];
    }
}
void spinfill_rgb888() {
    long i,j,window;
    j = 0;
    window=0;
    setWindow(window);
    while(j < 320*200*4) {
        VGA[i++] = spinvideobuffer[j++];
        VGA[i++] = spinvideobuffer[j++];
        VGA[i++] = spinvideobuffer[j++];
        j++;
        if(i>=65536) {
            window++;
            setWindow(window);
            i=0;
        }
    }
}
/*  pushes video buffer into vga card memory if under regular chunky mode;
    in unchained mode moves video display memory reading address*/
void spinpush_videobuffer() {
    if( videotype = RGBX8888 ) {
        spinfill_rgbX8888();
    } else if( videotype = RGB888 ) {
        spinfill_rgb888();
    } else {
        //uh-oh fake truecolor still not implemented
        int i=0;
    }
}
