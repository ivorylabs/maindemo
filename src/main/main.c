
#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <conio.h>
#include <mem.h>
#include <math.h>
#include <float.h>
#include "src\player\magic.h"
#include "src\beeper\beeper.h"
#include "src\timing\timing.h"
#include "src\memory\buffer.h"
#include "src\misc\midasdll.h"
#include "src\misc\muual.h"
#include "src\image\vgaimg.h"
#include "src\image\image.h"
#include "src\video\video.h"
#include "src\loader\loader.h"
#include "src\misc\definer.h"
#include "src\memory\buffer.h"
#include "src\png\lodepng.h"
#include "src\rasterizer\raster.h"
#include "src\rasterizer\simple_3D.h"
#include "src\deform\deform.h"
#include "src\model\model.h"
#include "src\particle\particle.h"
#include "src\voxel\voxel.h"
#include "src\misc\midasdll.h"
#include <conio.h>

MIDASmodule module;
MIDASmodulePlayHandle playHandle;

#define PI 3.14159265359f
byte videobuffer[320*400*3];
struct vec {
    float x;
    float y;
    float z;
};
void vgaSetBorder(int color);
#pragma aux vgaSetBorder = \
    "mov    dx,03DAh" \
    "in     al,dx" \
    "mov    dx,03C0h" \
    "mov    al,31h" \
    "out    dx,al" \
    "mov    al,bl" \
    "out    dx,al" \
    parm [ebx] \
    modify exact [eax edx];

#define Data    0x378
#define Status  0x379
#define Control 0x37a
extern struct vec uv_coords[80*50];
extern struct vec uv_tiny[40*25];
dword *videobuffer32=(dword *)&videobuffer;

byte spinvideobuffer[320*200*3];
dword *spinvideobuffer32=(dword *)&spinvideobuffer; 
long calls;
void MIDAS_CALL UpdateInfo(void) {   
    static MIDASplayStatus status;

    /* Get playback status: */
    if ( !MIDASgetPlayStatus(playHandle, &status) )
        MIDASerror();

    /* Store interesting information in easy-to-access variables: */
    position = status.position;
    pattern = status.pattern;
    row = status.row;
    syncInfo = status.syncInfo;
    calls++;
}

void begin() {
    printf("Please wait...");

    startTime = 0;
    currentTime = 0;
    prevTime = 0;
    passedTime = 0;
}

void end() {
}
/*Thanks for wbc^b-state for revealing this*/
void set60hz() {
    outp (0x3D4, 0x11); outp(0x3D5, (inp(0x3D5) & 0x7F));
    outp (0x3C2, 0xE3);
    outpw(0x3D4, 0x0B06); 
    outpw(0x3D4, 0x3E07);
    outpw(0x3D4, 0xC310);
    outpw(0x3D4, 0x8C11);
    outpw(0x3D4, 0x8F12);
    outpw(0x3D4, 0x9015);
    outpw(0x3D4, 0x0B16);
}
void calc_beat() {
    beat = beat + 1;
}
float pre_sin[1024];
void precalcsin() {
    float i=0.0f;
    int ii=0;
    for(i=0.0f; i<1024.0f; i++) {
        pre_sin[ii] = 0.5f + sin(3.141591f * 2.0f * (i/1024.0f))/2.0f;
        ii++;
    }
}
float screen_distance[25][40];
float screen_distancefull[200][3200];

void prescreendist() {
    float aspect = 200.0f / 320.0f;
    int _x,_y;
    for(_y=0; _y<25; _y++) {
        for(_x=0; _x<40; _x++) {
            screen_distance[_y][_x] = sqrt((_x/40.0f-0.5)*(_x/40.0f-0.5)+(_y/25.0f-0.5)*(_y/25.0f-0.5));
        }
    }
    for(_y=0; _y<200; _y++) {
        for(_x=0; _x<3200; _x++) {
            screen_distancefull[_y][_x] = sqrt((_x/320.0f-0.5)*(_x/320.0f-0.5)+(_y/200.0f-0.5)*(_y/200.0f-0.5));
        }
    }
}
float fasb(float a) {
    if(a<0.0f)
        return -a;
    return a;
}

unsigned char* decodeTwoSteps(const char* filename)
{
  unsigned error;
  unsigned char* image;
  unsigned width, height;
  unsigned char* png = 0;
  size_t pngsize;
  int i;

  error = lodepng_load_file(&png, &pngsize, filename);
  if(!error) error = lodepng_decode32(&image, &width, &height, png, pngsize);
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  free(png);

  //free(image);
  return image;
}
float val_a, val_b, val_c, val_d, val_e, val_f, val_g, val_h, val_i, val_j, rocket_row;
void MIDAS_CALL prevr(void) {
    update_rocketval();
    frameCount++;
    frameCount = rocket_row;
}
void update_rocketval() {
    int i, j;
        FILE *f;
        float buffer[11];
        int n;
        f = fopen("demomake.dat", "rb");
        if (f)
        {
            n = fread(buffer, 11, sizeof(float), f);
        }
        fclose (f);
    val_a = buffer[0];
    val_b = buffer[1];
    val_c = buffer[2];
    val_d = buffer[3];
    val_e = buffer[4];
    val_f = buffer[5];
    val_g = buffer[6];
    val_h = buffer[7];
    val_i = buffer[8];
    val_j = buffer[9];
    rocket_row = buffer[10];
}
int main(int argc, char *argv[]) {
    float page_appear = 0;
    int once = 0;
    int enddeemo = 0;
    int autotext = 0;
    int hz60true = 0;
    int begin_fadeexit = 0;
    int fadesound = 255;
    int r,g,b,i,j;
    byte *VGA=(byte *)0xA0000; 
    int window = 0;
    float frame = 0.0f;
    float fx, fy, fz;
    int ix, iy;
    struct Model model;
    float pos_obj[3]; 
    float rot_obj[3]; 
    float pos_cam[3]; 
    float rot_cam[3];

    unsigned char* image= decodeTwoSteps("img\\pro.png");
    unsigned char* height= decodeTwoSteps("img\\height.png");
    unsigned char* logo= decodeTwoSteps("img\\logo.png");
    unsigned char* isolam= decodeTwoSteps("img\\isolam.png");
    unsigned char* enviro= decodeTwoSteps("img\\enviro.png");
    unsigned char* waren= decodeTwoSteps("img\\waren.png");
    unsigned char* perhon= decodeTwoSteps("img\\perhon.png");
    unsigned char* bg= decodeTwoSteps("img\\bg.png");
    unsigned char* bb= decodeTwoSteps("img\\bb.png");
    unsigned char* puvesi= decodeTwoSteps("img\\puvesi.png");
    unsigned long *image32=(unsigned long *)image; 
    unsigned long *height32=(unsigned long *)height; 
    unsigned long *enviro32=(unsigned long *)enviro; 
    unsigned long *waren32=(unsigned long *)waren; 
    unsigned long *perhon32=(unsigned long *)perhon; 
    unsigned long *puvesi32=(unsigned long *)puvesi; 


    /* Prevent warnings: */
    argc = argc;
    argv = argv;
    MIDASstartup();
    if ( !MIDASconfig() )
    {
        /* Configuration failed. Check if it is an error - if yes, report,
               otherwise exit normally: */
        if ( MIDASgetLastError() )
        {
            MIDASerror();
        }
        else
        {
            printf("User exit!\n");
            return 1;
        }
    }

    if ( !MIDASinit() )
        MIDASerror();
    MIDASsetOption(MIDAS_OPTION_OUTPUTMODE, MIDAS_MODE_8BIT_MONO);
    MIDASsetOption(MIDAS_OPTION_MIXBUFLEN, 100);
    MIDASsetOption(MIDAS_OPTION_FILTER_MODE, MIDAS_FILTER_NONE);
    MIDASsetOption(MIDAS_OPTION_MIXING_MODE, MIDAS_MIX_NORMAL_QUALITY);
    
    
    if ( (module = MIDASloadModule("song.it")) == NULL )
        MIDASerror();

    if ( !MIDASsetTimerCallbacks(60000, TRUE, &prevr, NULL, NULL) )
        MIDASerror();



    *VGA = (int*)malloc(sizeof(int)*320*200*4);

    precalcsin();
    prescreendist();
    /*use image here*/
    /*
    for(i=0; i<320*200*4; i+=4) {
        videobuffer[i] = image[i+2];
        videobuffer[i+1] = image[i+1];
        videobuffer[i+2] = image[i];
        videobuffer[i+3] = image[i+3];
    }*/
    for(i=0; i<320*200*4; i+=4) {
        byte a = image[i+2];
        byte b = image[i+1];
        byte c = image[i];
        byte d = image[i+3];
        image[i] = a;
        image[i+1] = b;
        image[i+2] = c;
        image[i+3] = d;
    }
    for(i=0; i<640*400*4; i+=4) {
        byte a = isolam[i+2];
        byte b = isolam[i+1];
        byte c = isolam[i];
        byte d = isolam[i+3];
        isolam[i] = a;
        isolam[i+1] = b;
        isolam[i+2] = c;
        isolam[i+3] = d;
    }
    for(i=0; i<1024*1024*4; i+=4) {
        byte a = waren[i+2];
        byte b = waren[i+1];
        byte c = waren[i];
        byte d = waren[i+3];
        waren[i] = a;
        waren[i+1] = b;
        waren[i+2] = c;
        waren[i+3] = d;
    }
    for(i=0; i<640*200*4; i+=4) {
        byte a = perhon[i+2];
        byte b = perhon[i+1];
        byte c = perhon[i];
        byte d = perhon[i+3];
        perhon[i] = a;
        perhon[i+1] = b;
        perhon[i+2] = c;
        perhon[i+3] = d;
    }
    for(i=0; i<640*200*4; i+=4) {
        byte a = puvesi[i+2];
        byte b = puvesi[i+1];
        byte c = puvesi[i];
        byte d = puvesi[i+3];
        puvesi[i] = a;
        puvesi[i+1] = b;
        puvesi[i+2] = c;
        puvesi[i+3] = d;
    }
    gen_texture(image);
    /* Unbuffer stdout: */
    setbuf(stdout, NULL);


    model = loadModel("models\\s_verts.bin", "models\\s_faces.bin");
    

    if ( (playHandle = MIDASplayModule(module, TRUE)) == 0 )
        MIDASerror();
    begin();

    sm(0x010f);
    if(hz60true)
        set60hz();

    once = 1;

    i = 0;
    

    videotype = RGBX8888;
    vgaSetBorder(0);
    MIDASsetMusicVolume(playHandle,0);
    ////////////////////
    ////////////////////
    while ( enddeemo == 0 ) {
        float aspect = 200.0f / 320.0f;
        if(kbhit()) {
            char key = _getch();
            if(key == 27) {
                enddeemo=100;
            }
        }
        i=0;
        ix=0;
        iy=0;
        
        if(((int)val_a)==19) {
            for(iy=0; iy<100; iy++) {
                for(ix=0; ix<320; ix++) {
                    videobuffer[ix*4+(100+iy)*320*4+0] -= videobuffer[ix*4+(100-iy)*320*4+0] << 1  + videobuffer[ix*4+(100-iy)*320*4+4] << 1 ;
                    videobuffer[ix*4+(100+iy)*320*4+1] -= videobuffer[ix*4+(100-iy)*320*4+1] << 1  + videobuffer[ix*4+(100-iy)*320*4+5] << 1 ;
                    videobuffer[ix*4+(100+iy)*320*4+2] -= videobuffer[ix*4+(100-iy)*320*4+2] << 1  + videobuffer[ix*4+(100-iy)*320*4+6] << 1 ;
                    videobuffer[ix*4+(100+iy)*320*4+3] = 0;
                }
            }
            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] = videobuffer[i+0] >> 1;
                videobuffer[i+1] = videobuffer[i+1] >> 1;
                videobuffer[i+2] = videobuffer[i+2] >> 1;
                videobuffer[i+3] = 0;
            }
            j=320*100*4;
            for(i=320*100*4; i<320*200*4; i+=4) {
                videobuffer[i+0] = videobuffer[j+0] >> 2 + videobuffer[j+0] >> 2;
                videobuffer[i+1] = videobuffer[j+1] >> 2 + videobuffer[j+4] >> 2;
                videobuffer[i+2] = videobuffer[j+2] >> 2 + videobuffer[j-2] >> 2;
                videobuffer[i+3] = 0;
                j-=4;
            }
            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = val_b;
            pos_cam[1] = val_c;
            pos_cam[2] = val_d;
            rot_cam[0] = val_e;
            rot_cam[1] = val_f;
            rot_cam[2] = val_g;

            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            for(fy = 6.0f; fy < 32.0f; fy++) {
                for(fx = 8.0f; fx < 40.0f; fx++) {
                    float a[3];
                    int tex_x = (int)(fx*4.0f)*4;
                    int tex_y = (int)(fy*4.0f)*640*4;
                    a[0] = cos(fx*2.0*3.14159/48.)*cos(fy*2.0*3.14159/64.)*16.0f;
                    a[1] = sin(fy*2.0*3.14159/64.)*16.0f*(val_h+sin(fx*2.0f*3.141592/16.0f)/3.0f);
                    a[2] = sin(fx*2.0*3.14159/48.)*cos(fy*2.0*3.14159/64.)*16.0f;
                    drawParticle( a[0], a[1], a[2], (puvesi[tex_x+tex_y/2]>>4)+0x0100*(puvesi[tex_x+tex_y/2+1]>>4)+0x010000*(puvesi[tex_x+tex_y/2+2]>>4));
                } 
            }
            for(i=0; i<320*200*4; i+=4) {
                spinvideobuffer[i] = max( -(videobuffer[i]) + puvesi[i],0);
                spinvideobuffer[i+1] = max( -(videobuffer[i+1]) + puvesi[i+1],0);
                spinvideobuffer[i+2] = max( -(videobuffer[i+2]) + puvesi[i+2],0);
                spinvideobuffer[i+3] = 0;
            }
            spinpush_videobuffer();
        } else if(((int)val_a)==20) {

            for(i=0; i<320*200; i++) {
                videobuffer32[i] = 0;
            }
            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = val_b;
            pos_cam[1] = val_c;
            pos_cam[2] = val_d;
            rot_cam[0] = val_e;
            rot_cam[1] = val_f;
            rot_cam[2] = val_g;

            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
                for(fx = 0.0f; fx < 320.0f; fx+=4.0f) {
                    float a[3];
                    a[0] = sin(163123.23235f*fx)*8.0f;
                    a[1] = -fmod(fx*0.42342f+frame*0.06f*(1.0f+sin(fx*0.1f)/3.0f),16.0f)+8.0f;
                    a[2] = cos(123121.75635f*fx)*8.0f;
                    drawParticle( a[0], a[1], a[2], 0x1f1204+fx/10.0f);
                } 
            
            i=0;
            for(iy=0; iy<320*200*4; iy+=320*4) {
            j=iy;
            for(ix=0; ix<320*4; ix+=4) {
                videobuffer[i] = (videobuffer[i]&bb[j+0]) + bg[j+0];
                videobuffer[i+1] = (videobuffer[i+1]&bb[j+1]) + bg[j+1];
                videobuffer[i+2] = (videobuffer[i+2]&bb[j+2]) + bg[j+2];
                videobuffer[i+3] = 0;
                i+=4;
                j+=4;
            }
            }
            push_videobuffer();
        } else if(((int)val_a)==14) {

            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] -= videobuffer[i+0] >> 2;
                videobuffer[i+1] -= videobuffer[i+1] >> 2;
                videobuffer[i+2] -= videobuffer[i+2] >> 2;
                videobuffer[i+3] = 0;
            }
            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = val_b;
            pos_cam[1] = val_c;
            pos_cam[2] = val_d;
            rot_cam[0] = val_e;
            rot_cam[1] = val_f;
            rot_cam[2] = val_g;

            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            for(fy = 6.0f; fy < 32.0f; fy++) {
                for(fx = 8.0f; fx < 40.0f; fx++) {
                    float a[3];
                    int tex_x = (int)(fx*4.0f)*4;
                    int tex_y = (int)(fy*4.0f)*640*4;
                    a[0] = cos(fx*2.0*3.14159/48.)*cos(fy*2.0*3.14159/64.)*16.0f;
                    a[1] = sin(fy*2.0*3.14159/64.)*16.0f*(val_h+sin(fx*2.0f*3.141592/16.0f)/3.0f);
                    a[2] = sin(fx*2.0*3.14159/48.)*cos(fy*2.0*3.14159/64.)*16.0f;
                    drawParticle( a[0], a[1], a[2], (perhon[tex_x+tex_y/2]>>4)+0x0100*(perhon[tex_x+tex_y/2+1]>>4)+0x010000*(perhon[tex_x+tex_y/2+2]>>4));
                } 
            }
            for(i=0; i<320*200*4; i+=4) {
                spinvideobuffer[i] = max( -(videobuffer[i]) + perhon[i],0);
                spinvideobuffer[i+1] = max( -(videobuffer[i+1]) + perhon[i+1],0);
                spinvideobuffer[i+2] = max( -(videobuffer[i+2]) + perhon[i+2],0);
                spinvideobuffer[i+3] = 0;
            }
            spinpush_videobuffer();
        } else if(((int)val_a)==15) {

            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] = videobuffer[i+0] >> 2;
                videobuffer[i+1] = videobuffer[i+1] >> 2;
                videobuffer[i+2] = videobuffer[i+2] >> 2;
                videobuffer[i+3] = 0;
            }
            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = val_b;
            pos_cam[1] = val_c;
            pos_cam[2] = val_d;
            rot_cam[0] = val_e;
            rot_cam[1] = val_f;
            rot_cam[2] = val_g;

            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            for(fy=0.0f; fy<190; fy+=3) {
                for(fx=0.0f; fx<640; fx+=3) {
                    int tex_x = (int)(fx)*4;
                    int tex_y = (int)(fy)*640*4;
                    if(logo[tex_x+tex_y]>0)
                    drawParticle( (fx-320)/10.0f, (fy-100)/10.0f, 0.0f, (perhon[tex_x+tex_y/2]>>4)+0x0100*(perhon[tex_x+tex_y/2+1]>>4)+0x010000*(perhon[tex_x+tex_y/2+2]>>4));
                }
            }
            for(fz=0.0f; fz<16.0f; fz++)
            for(fy=0.0f; fy<190; fy+=8) {
                for(fx=0.0f; fx<640; fx+=8) {
                    int tex_x = (int)(fx+sin(fz*1232.1321)*6+6)*4;
                    int tex_y = (int)(fy+sin(fz*44.21)*6+6)*640*4;
                    float ffffz = fmod(fz+val_h,16.0f);
                    if(logo[tex_x+tex_y]>0)
                        drawParticle( (1.0f+ffffz*0.02f)*(fx-320+sin(fz*1232.1321)*6+6)/10.0f, (1.0f+ffffz*0.02f)*(fy-100+sin(fz*44.21)*6+6)/10.0f, -ffffz, (perhon[tex_x+tex_y/2]>>4)+0x0100*(perhon[tex_x+tex_y/2+1]>>4)+0x010000*(perhon[tex_x+tex_y/2+2]>>4));
                }
            }
            push_videobuffer();
        } else if(((int)val_a)==0) {
            for(i=0; i<320*200; i++) {
                videobuffer32[i] = 0;
            }
            pos_cam[0] = 3.0f*cos(frame*0.1f);
            pos_cam[1] = 32.0f+sin(frame*0.03f);
            pos_cam[2] = 40.0f;
            rot_cam[0] = cos(frame*0.1f)/7.0f;
            rot_cam[1] = 0.3f+sin(frame*0.03f)/4.0f;
            rot_cam[2] = cos(frame*0.02123f)/8.0f;
            voxeltransformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam, 90.0f);
            for(fy=80.0f; fy>0.0f; fy--) {
                int tex_y = ((int)(fy+24+frame*4.0f))&1023;
                int indx = (int)((int)(sin(frame*0.02123f)*100.0f)-48+200)*4+tex_y*1024*4;
                for(fx=-50.0f; fx<50.0f; fx++) {
                    int tex_x = (int)((int)(sin(frame*0.02123f)*100.0f)+fx+200);
                    unsigned long c = (waren[indx] * enviro[indx])/64;
                    indx++;
                    c += ((waren[indx] * enviro[indx])/64) * 0x0100;
                    indx++;
                    c += ((waren[indx] * enviro[indx])/64) * 0x010000;
                    indx+=2;
                    voxalate( fx*1.3f, -enviro[tex_x*4+tex_y*1024*4+2]/6.0f, fy*3.3f, c);
                }
            }
            fillholesvoxel();
            push_videobuffer();
        }else if(((int)val_a)==1) {
            for(i=0; i<320*200; i++) {
                videobuffer32[i] = 0;
            }
            pos_cam[0] = cos(frame*0.01f)/8.0f;
            pos_cam[1] = sin(frame*0.01f)+7.0f;
            pos_cam[2] = 32.0f;
            rot_cam[0] = cos(frame*0.01f)/8.0f;
            rot_cam[1] = 0.4f;
            rot_cam[2] = 0.0f;
            voxeltransformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam,100.0f);
            for(fy=90.0f; fy>0.0f; fy--) {
                int tex_y = ((int)(fy+24+frame*4.0f))&1023;
                int indx = (int)((int)(sin(frame*0.02123f)*100.0f)-48+200)*4+tex_y*1024*4;
                for(fx=-48.0f; fx<48.0f; fx++) {
                    int tex_x = (int)((int)(sin(frame*0.02123f)*100.0f)+fx+200);
                    float road=(40.0f-enviro[indx]/6.0f);
                    unsigned long c = (waren[indx] * enviro[indx])/64;
                    indx++;
                    c += ((waren[indx] * enviro[indx])/64) * 0x0100;
                    indx++;
                    c += ((waren[indx] * enviro[indx])/64) * 0x010000;
                    indx+=2;
                    voxalate( sin(3.141592f*2.0f*fx/140.0f)*road, cos(3.141592f*2.0f*fx/140.0f)*road, fy*3.3f, c);
                }
            }
            fillholesvoxel();
            push_videobuffer();
        } else if(((int)val_a)==2) {
            for(fy = 0.0f; fy < 25.0f; fy++) {
                ix=0;
                for(fx = 0.0f; fx < 40.0f; fx++) {
                    struct vec temp;
                    struct vec d;
                    struct vec r;
                    float dist = screen_distance[iy][ix];
                    int step;
                    int collision = 0;
                    temp.x = (fx / 40.0f - 0.5f) ;
                    temp.y = (fy / 25.0f - 0.55f) *aspect;
                    temp.z = 1.0f;
                    temp.x *= val_b;
                    temp.y *= val_b;
                    d.x = temp.x * (1.1f+val_j*sin(val_d)) + (sin(val_f)/10.0f+0.16f)*sin(atan2(temp.x,temp.y)*2.0f*val_c+val_e);
                    d.y = temp.y * (1.1f+val_j*sin(val_d)) + (sin(val_f)/10.0f+0.16f)*sin(atan2(temp.x,temp.y)*2.0f*val_c+val_e);
                    d.x = d.x + (1.1f+val_i*sin(val_d)) + 0.1f*sin(atan2(d.x,d.y)*2.0f*val_c+val_e);
                    d.y = d.y + (1.1f+val_i*sin(val_d)) + 0.1f*sin(atan2(d.x,d.y)*2.0f*val_c+val_e);
                    d.y-=val_g;
                    d.z= (1.1f+val_i*sin(val_d)) ;
                    uv_tiny[i++] = d;

                    ix++;
                }
                iy++;
            }
            stretch_uv( 320, 200);
            push_videobuffer();
        } else if(((int)val_a)==3) {
            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] = videobuffer[i+0] >> 2;
                videobuffer[i+1] = videobuffer[i+1] >> 2;
                videobuffer[i+2] = videobuffer[i+2] >> 2;
                videobuffer[i+3] = 0;
            }
            i=0;
            for(fy = 0.0f; fy < 25.0f; fy++) {
                ix=0;
                for(fx = 0.0f; fx < 40.0f; fx++) {
                    float angle;
                    struct vec temp;
                    struct vec temp2;
                    struct vec d;
                    struct vec r;
                    float dist = screen_distance[iy][ix];
                    int step;
                    int collision = 0;
                    temp2.x = (fx / 40.0f - 0.5f) ;
                    temp2.y = (fy / 25.0f - 2.4f) * aspect;
                    angle = val_b + sin(temp2.x*val_i)*val_j;
                    temp2.y += val_c / 2.0f;
                    d.z = fabs( temp.y + 1.0f + sin(frame*0.03f)/64.0f );
                    temp.x = temp2.x * cos(angle) - temp2.y * sin(angle);
                    temp.y = temp2.x * sin(angle) + temp2.y * cos(angle);
                    d.y = 0.115f / d.z;
                    d.x = temp.x * d.y;
                    d.y+=val_e;
                    d.x+=val_d;
                    d.z=max(0.9f-d.z*val_g,0.3f);
                    d.z*=val_f;
                    if(d.z < 0.08f) {
                        d.x = 1.0f;
                        d.y = 0.5f;
                    }
                    uv_tiny[i++] = d;

                    ix++;
                }
                iy++;
            }
            add_stretch_uv( 320, 200);
            push_videobuffer();
        } else if(((int)val_a)==4) {

            for(i=0; i<320*200*4; i+=4) { 
                videobuffer[i+0] = videobuffer[i+0] >> 1;
                videobuffer[i+1] = videobuffer[i+1] >> 1;
                videobuffer[i+2] = videobuffer[i+2] >> 1;
                videobuffer[i+3] = 0;
            }
            i=0;
                for(fy = 0.0f; fy < 25.0f; fy++) {
                    ix=0;
                    for(fx = 0.0f; fx < 40.0f; fx++) {
                        
                        struct vec d ;
                        struct vec temp;
                        float t = frame*0.1f;
                        float angle, shape, m, n1, n2, n3, rpart, apart, bpart;
                        struct vec symmetry;

                        temp.x = (fx / 40.0f - 0.5f) ;
                        temp.y = (fy / 25.0f - 0.5f) * aspect;

                        d.x = temp.x ;
                        d.y = temp.y ;
                        temp.x += sin(val_d)/val_c;
                        temp.y += cos(val_e)/val_c;

                        angle = atan2(temp.x, temp.y);

                        d.z = sqrt(temp.x*temp.x+temp.y*temp.y) ;

                        d.y = val_h+atan2(temp.x,temp.y)/4.0f;
                        d.x = (val_i+1.0f+val_b*0.1*sin(angle*val_f))*0.2f / d.z ;
                        d.z = val_j*(val_i+1.0f+sin(angle*val_f))+12.0f*max(1.0f-3.0f*sqrt(temp.x*temp.x+temp.y*temp.y),0) ;
                        d.x+= val_g;
                        uv_tiny[i++] = d;

                        ix++;
                    }
                    iy++;
                }
                add_stretch_uv( 320, 200);
            push_videobuffer();
        } else if(((int)val_a)==16) {

            for(i=0; i<320*200*4; i+=4) { 
                videobuffer[i+0] -= videobuffer[i+0] >> 2;
                videobuffer[i+1] -= videobuffer[i+1] >> 2;
                videobuffer[i+2] -= videobuffer[i+2] >> 2;
                videobuffer[i+3] = 0;
            }
            i=0;
                reset_raster();
                setFilled(0);
                setLined(1);
                for(fy = 0.0f; fy < 12.0f; fy++) 
                for(fx = 0.0f; fx < 12.0f; fx++) {
                    float a[3], b[3], c[3];
                    a[0] = sin(2.0f*3.141592f*fx/12.0f)*12.0f;
                    a[1] = cos(2.0f*3.141592f*fx/12.0f)*12.0f;
                    a[2] = 0.0f+(fmod(fy*2.0f+frame*0.1f,24.0f)-12.0f)*8.0f;
                    b[0] = sin(2.0f*3.141592f*fx/12.0f)*12.0f;
                    b[1] = cos(2.0f*3.141592f*fx/12.0f)*12.0f;
                    b[2] = 16.0f+(fmod(fy*2.0f+frame*0.1f,24.0f)-12.0f)*8.0f;
                    c[0] = sin(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    c[1] = cos(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    c[2] = 0.0f+(fmod(fy*2.0f+frame*0.1f,24.0f)-12.0f)*8.0f;
                    i += 3;
                    pushPolygon( a, b, c, 32, 32, 32);
                } 


                pos_obj[0] = 0.0f;
                pos_obj[1] = 0.0f;
                pos_obj[2] = 0.0f;
                rot_obj[0] = 0.0f;
                rot_obj[1] = 0.0f;
                rot_obj[2] = 0.0f;
                pos_cam[0] = 0.0f;
                pos_cam[1] = 0.0f;
                pos_cam[2] = 0.0f;
                rot_cam[0] = sin(val_d)/val_c;
                rot_cam[1] = cos(val_e)/val_c;
                rot_cam[2] = val_i;
                end3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
                rasterize();

                reset_raster();
                setFilled(1);
                setLined(0);
                for(fy = 0.0f; fy < 24.0f; fy++)
                for(fx = 0.0f; fx < 12.0f; fx+=4) {
                    float a[3], b[3], c[3];
                    a[0] = sin(2.0f*3.141592f*fx/12.0f)*12.0f;
                    a[1] = cos(2.0f*3.141592f*fx/12.0f)*12.0f;
                    a[2] = 0.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                    b[0] = sin(2.0f*3.141592f*fx/12.0f)*12.0f;
                    b[1] = cos(2.0f*3.141592f*fx/12.0f)*12.0f;
                    b[2] = 8.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                    c[0] = sin(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    c[1] = cos(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    c[2] = 0.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                    i += 3;
                    pushPolygon( a, b, c, 20, 10, 0);
                }  
                for(fy = 0.0f; fy < 24.0f; fy++)
                for(fx = 0.0f; fx < 12.0f; fx+=4) {
                    float a[3], b[3], c[3];
                    a[0] = sin(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    a[1] = cos(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    a[2] = 8.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                    b[0] = sin(2.0f*3.141592f*fx/12.0f)*12.0f;
                    b[1] = cos(2.0f*3.141592f*fx/12.0f)*12.0f;
                    b[2] = 8.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                    c[0] = sin(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    c[1] = cos(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                    c[2] = 0.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                    i += 3;
                    pushPolygon( a, b, c, 20, 10, 0);
                }  


                pos_obj[0] = 0.0f;
                pos_obj[1] = 0.0f;
                pos_obj[2] = 0.0f;
                rot_obj[0] = 0.0f;
                rot_obj[1] = 0.0f;
                rot_obj[2] = 0.0f;
                pos_cam[0] = 0.0f;
                pos_cam[1] = 0.0f;
                pos_cam[2] = 0.0f;
                rot_cam[0] = sin(frame*0.03754f)/4.0f;
                rot_cam[1] = cos(frame*0.0212342f)/4.0f;
                rot_cam[2] = 0.0f;
                end3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
                rasterize();
            push_videobuffer();
        }else if(((int)val_a)==5) {
            /*

                grass
                scene

            */
            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] = videobuffer[i+0] >> 1;
                videobuffer[i+1] = videobuffer[i+1] >> 1;
                videobuffer[i+2] = videobuffer[i+2] >> 1;
                videobuffer[i+3] = 0;
            }
            i=0;


            reset_raster();
            setFilled(0);
            setLined(1);


            for(fy=0.0; fy<12.; fy++)
            for(fx=0.0; fx<6.; fx++) {
                float a[3], b[3], c[3];
                a[0] = cos(fx*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                a[1] = sin(fy*2.0*3.14159/12.);
                a[2] = sin(fx*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                b[0] = cos((fx+1.0f)*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                b[1] = sin(fy*2.0*3.14159/12.);
                b[2] = sin((fx+1.0f)*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                c[0] = cos(fx*2.0*3.14159/12.)*cos((fy+1.0f)*2.0*3.14159/12.);
                c[1] = sin((fy+1.0f)*2.0*3.14159/12.);
                c[2] = sin(fx*2.0*3.14159/12.)*cos((fy+1.0f)*2.0*3.14159/12.);
                a[1] *= sin(a[1]+frame*0.1231f)/3.0f+0.5f;
                b[1] *= sin(b[1]+frame*0.1231f)/3.0f+0.5f;
                c[1] *= sin(c[1]+frame*0.1231f)/3.0f+0.5f;
                a[0] *= 1.0f + sin(a[1]*3.0f+frame*0.1f)/3.0f;
                a[3] *= 1.0f + sin(a[1]*3.0f+frame*0.1f)/3.0f;
                b[0] *= 1.0f + sin(b[1]*3.0f+frame*0.1f)/3.0f;
                b[3] *= 1.0f + sin(b[1]*3.0f+frame*0.1f)/3.0f;
                c[0] *= 1.0f + sin(c[1]*3.0f+frame*0.1f)/3.0f;
                c[3] *= 1.0f + sin(c[1]*3.0f+frame*0.1f)/3.0f;
                a[0] *= 6.0f;
                a[1] *= 6.0f;
                a[2] *= 6.0f;
                b[0] *= 6.0f;
                b[1] *= 6.0f;
                b[2] *= 6.0f;
                c[0] *= 6.0f;
                c[1] *= 6.0f;
                c[2] *= 6.0f;

                pushPolygon( a, b, c, 06 + (int)(fy * 12.0f) , (int)(fx*9.0f)+2, 1+(int)(fx*12.0f));
            }
            for(fy=0.0; fy<12.; fy++)
            for(fx=6.0; fx<12.; fx++) {
                float a[3], b[3], c[3];
                a[0] = cos((fx+1.0f)*2.0*3.14159/12.)*cos((fy+1.0f)*2.0*3.14159/12.);
                a[1] = sin((fy+1.0f)*2.0*3.14159/12.);
                a[2] = sin((fx+1.0f)*2.0*3.14159/12.)*cos((fy+1.0f)*2.0*3.14159/12.);
                b[0] = cos((fx+1.0f)*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                b[1] = sin(fy*2.0*3.14159/12.);
                b[2] = sin((fx+1.0f)*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                c[0] = cos(fx*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                c[1] = sin(fy*2.0*3.14159/12.);
                c[2] = sin(fx*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                a[1] *= sin(a[1]+frame*0.1231f)/3.0f+0.5f;
                b[1] *= sin(b[1]+frame*0.1231f)/3.0f+0.5f;
                c[1] *= sin(c[1]+frame*0.1231f)/3.0f+0.5f;
                a[0] *= 1.0f + sin(a[1]*3.0f+frame*0.1f)/3.0f;
                a[3] *= 1.0f + sin(a[1]*3.0f+frame*0.1f)/3.0f;
                b[0] *= 1.0f + sin(b[1]*3.0f+frame*0.1f)/3.0f;
                b[3] *= 1.0f + sin(b[1]*3.0f+frame*0.1f)/3.0f;
                c[0] *= 1.0f + sin(c[1]*3.0f+frame*0.1f)/3.0f;
                c[3] *= 1.0f + sin(c[1]*3.0f+frame*0.1f)/3.0f;
                a[0] *= 6.0f;
                a[1] *= 6.0f;
                a[2] *= 6.0f;
                b[0] *= 6.0f;
                b[1] *= 6.0f;
                b[2] *= 6.0f;
                c[0] *= 6.0f;
                c[1] *= 6.0f;
                c[2] *= 6.0f;
                pushPolygon( a, b, c, 06 + (int)(fy * 12.0f) , (int)(fx*9.0f)+2, 1+(int)(fx*12.0f));
            }

            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = frame*0.031f;
            rot_obj[1] = frame*0.02112313f;
            rot_obj[2] = frame*0.01532f;
            pos_cam[0] = 0.0f;
            pos_cam[1] = 0.0f;
            pos_cam[2] = 12.0f;
            rot_cam[0] = 0.0f;
            rot_cam[1] = 0.0f;
            rot_cam[2] = 0.0f;

            sortedend3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            noalpha_rasterize();



            push_videobuffer();
        
        } else if(((int)val_a)==6) {
            reset_raster();
            setFilled(0);
            setLined(1);
            
            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = val_b;
            pos_cam[1] = val_c;
            pos_cam[2] = val_d;
            rot_cam[0] = val_e;
            rot_cam[1] = val_f;
            rot_cam[2] = val_g;

            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] -= videobuffer[i+0] >> 2;
                videobuffer[i+1] -= videobuffer[i+1] >> 1;
                videobuffer[i+2] -= videobuffer[i+2] >> 1;
                videobuffer[i+3] = 0;
            }
            for(fx=-8.0f; fx<8.0f; fx++) {
                for(fy=-8.0f; fy<8.0f; fy++) {
                    float p0[3], p1[3], p2[3];
                    int tex_x = ((int)fx+32);
                    int tex_y = ((int)fy+32);
                    p0[0] = fx*2.0f;
                    p0[1] = 2.0f+sin(enviro[tex_x*4+tex_y*4*1024+1]/14.0f+frame*val_h)*val_i;
                    p0[2] = fy*2.0f;

                    p1[0] = fx*2.0f+2.0f;
                    p1[1] = 2.0f+sin(enviro[(tex_x+1)*4+tex_y*4*1024+1]/14.0f+frame*val_h)*val_i;
                    p1[2] = fy*2.0f;

                    p2[0] = fx*2.0f;
                    p2[1] = 2.0f+sin(enviro[tex_x*4+(tex_y+1)*4*1024+1]/14.0f+frame*val_h)*val_i;
                    p2[2] = fy*2.0f+2.0f;


                    pushPolygon(p0, p1, p2, waren[tex_x*4+(tex_y+1)*4*1024+0], waren[tex_x*4+(tex_y+1)*4*1024+1], waren[tex_x*4+(tex_y+1)*4*1024+2]);

                }
            }

            if(val_j>0.07f) {
                for(fy=0.0; fy<12.; fy++)
                for(fx=0.0; fx<6.; fx++) {
                    float a[3], b[3], c[3];
                    a[0] = cos(fx*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                    a[1] = sin(fy*2.0*3.14159/12.);
                    a[2] = sin(fx*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                    b[0] = cos((fx+1.0f)*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                    b[1] = sin(fy*2.0*3.14159/12.);
                    b[2] = sin((fx+1.0f)*2.0*3.14159/12.)*cos(fy*2.0*3.14159/12.);
                    c[0] = cos(fx*2.0*3.14159/12.)*cos((fy+1.0f)*2.0*3.14159/12.);
                    c[1] = sin((fy+1.0f)*2.0*3.14159/12.);
                    c[2] = sin(fx*2.0*3.14159/12.)*cos((fy+1.0f)*2.0*3.14159/12.);
                    a[1] *= sin(a[1]+frame*0.1231f)/3.0f+0.5f;
                    b[1] *= sin(b[1]+frame*0.1231f)/3.0f+0.5f;
                    c[1] *= sin(c[1]+frame*0.1231f)/3.0f+0.5f;
                    a[0] *= 1.0f + sin(a[1]*3.0f+a[0]*3.0f+frame*0.1f)/5.0f;
                    a[3] *= 1.0f + sin(a[1]*3.0f+a[0]*3.0f+frame*0.1f)/5.0f;
                    b[0] *= 1.0f + sin(b[1]*3.0f+b[0]*3.0f+frame*0.1f)/5.0f;
                    b[3] *= 1.0f + sin(b[1]*3.0f+b[0]*3.0f+frame*0.1f)/5.0f;
                    c[0] *= 1.0f + sin(c[1]*3.0f+c[0]*3.0f+frame*0.1f)/5.0f;
                    c[3] *= 1.0f + sin(c[1]*3.0f+c[0]*3.0f+frame*0.1f)/5.0f;
                    a[0] *= val_j;
                    a[1] *= val_j;
                    a[2] *= val_j;
                    b[0] *= val_j;
                    b[1] *= val_j;
                    b[2] *= val_j;
                    c[0] *= val_j;
                    c[1] *= val_j;
                    c[2] *= val_j;
                    pushPolygon( a, b, c, 153, sin(fx*0.11421f+frame*0.1f)*32+220, sin(fy*0.119421f+frame*0.03f)*127+127);
                }
            }


            end3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            rasterize();

            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            for(fx=0.0f; fx<50.0f; fx++)
                drawParticle( sin(fx*234234.2342341f*fx)*16.0f, fmod(fx-(frame+sin(fx)/10.0f)*0.16f,64.0f)-32.0f, cos(fx*1231.121*fx)*16.0f, 0x001f0500+(int)(12.0f+11.0f*sin(fx)));
            for(fx=50.0f; fx<100.0f; fx++)
                drawParticle( sin(fx*234234.2342341f*fx)*16.0f, fmod(fx-(frame+sin(fx)/10.0f)*0.16f,64.0f)-32.0f, cos(fx*1231.121*fx)*16.0f, 0x001f1510+(int)(12.0f+11.0f*sin(fx)));

            push_videobuffer();
        } else if(((int)val_a)==11) {
            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] -= videobuffer[i+0] >> 1;
                videobuffer[i+1] -= videobuffer[i+1] >> 2;
                videobuffer[i+2] -= videobuffer[i+2] >> 3;
                videobuffer[i+3] = 0;
            }
            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = 0.0f;
            pos_cam[1] = 0.0f;
            pos_cam[2] = 12.0f;
            rot_cam[0] = frame*0.01f;
            rot_cam[1] = 0.0f;
            rot_cam[2] = 0.0f;
            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            for(fx=0.0f; fx<1000.0f; fx++)
                drawParticle( fmod(frame*0.1f+fx/5.0f,64.0f)-32.0f, 3.0f*(cos(frame*0.1f+fx/64.0f)+cos(fx/4.0f)), 3.0f*sin(frame*0.1f+fx/64.0f), 0x09020509);
            push_videobuffer();
        } else if(((int)val_a)==7) {
            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] -= videobuffer[i+0] >> 1;
                videobuffer[i+1] -= videobuffer[i+1] >> 1;
                videobuffer[i+2] -= videobuffer[i+2] >> 1;
                videobuffer[i+3] = 0;
            }
            pos_obj[0] = 4.0f-frame*0.01f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = 0.0f;
            pos_cam[1] = 0.0f;
            pos_cam[2] = 5.0f+sin(frame*0.003)*2.0f;
            rot_cam[0] = frame*0.01f;
            rot_cam[1] = sin(frame*0.0023123f);
            rot_cam[2] = cos(frame*0.0017573f);
            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            for(fz=0.0f; fz<30.0f; fz++)
            for(fy=1.0f; fy<12.0f; fy++)
            for(fx=3.0f; fx<9.0f; fx++) {
                float mz = fmod(fz + frame*0.4f,30.0f)+2.0f;
                float mx = fmod(fx + mz*0.1f - frame*0.0321f,12.0f) + sin(mz*0.01f)*4.0f;
                float my = fmod(fy + mz*0.1f - frame*0.0321f,12.0f) + cos(mz*0.01f)*4.0f;
                float fmx = cos(mx*2.0*3.141592f/12.0f)*cos(my*2.0f*3.14159f/12.0f)*mz/4.0f;
                float fmy = sin(my*2.0*3.141592f/12.0f)*mz/4.0f;
                float fmz = sin(mx*2.0*3.141592f/12.0f)*cos(my*2.0f*3.14159f/12.0f)*mz/4.0f;
                drawParticle(fmx, fmy, fmz, 0x00060201 + (int)(fz / 2.0f) + 0x0100 * (int)(mx/4.0f) + 0x010000 * (int)(my/5.0f));
            }
            push_videobuffer();
        } else if(((int)val_a)==8) {
            float a[3], b[3], c[3];
            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] -= videobuffer[i+0] >> 3 ;
                videobuffer[i+1] -= videobuffer[i+1] >> 3 ;
                videobuffer[i+2] -= videobuffer[i+2] >> 2 ;
                videobuffer[i+3] = 0;
            }
            
            reset_raster();
            setFilled(0);
            setLined(1);
            for(fx=0.0f; fx<20.0f; fx++) {
                a[0] = -1.0f;
                a[1] = 0.666f;
                a[2] = 0.0f;
                b[0] = 1.0f;
                b[1] = 0.666f;
                b[2] = 0.0f;
                c[0] = 0.0f;
                c[1] = -1.0f;
                c[2] = 0.0f;
                a[0] += sin(fx*34.0123123f) * 12.0f;
                b[0] += sin(fx*34.0123123f) * 12.0f;
                c[0] += sin(fx*34.0123123f) * 12.0f;
                a[1] += cos(fx*33.123123f) * 12.0f;
                b[1] += cos(fx*33.123123f) * 12.0f;
                c[1] += cos(fx*33.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f+0.5f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f+0.5f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f+0.5f,64.0f)-32.0f;
                pushPolygon( a, b, c, fx/2.0f, 80+sin(fx/33.0f)*30, 80-abs(a[3])*3.0f);
            }
            for(fx=0.0f; fx<20.0f; fx++) {
                a[0] = -1.0f;
                a[1] = 0.666f;
                a[2] = 0.0f;
                b[0] = 1.0f;
                b[1] = 0.666f;
                b[2] = 0.0f;
                c[0] = 0.0f;
                c[1] = -1.0f;
                c[2] = 0.0f;
                a[0] += sin(fx*11.0123123f) * 12.0f;
                b[0] += sin(fx*11.0123123f) * 12.0f;
                c[0] += sin(fx*11.0123123f) * 12.0f;
                a[1] += cos(fx*44.123123f) * 12.0f;
                b[1] += cos(fx*44.123123f) * 12.0f;
                c[1] += cos(fx*44.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                pushPolygon( a, b, c, 80+sin(fx/10.0f)*50, fx/3.0f, 80-abs(a[3])*3.0f);
            }
            for(fx=0.0f; fx<20.0f; fx++) {
                a[0] = 1.0f;
                a[1] = -1.0f;
                a[2] = 0.0f;
                b[0] = 1.0f;
                b[1] = -1.0f;
                b[2] = 0.0f;
                c[0] = -1.0f;
                c[1] = 1.0f;
                c[2] = 1.0f;
                a[0] += sin(fx*12.0123123f) * 12.0f;
                b[0] += sin(fx*12.0123123f) * 12.0f;
                c[0] += sin(fx*12.0123123f) * 12.0f;
                a[1] += cos(fx*33.123123f) * 12.0f;
                b[1] += cos(fx*33.123123f) * 12.0f;
                c[1] += cos(fx*33.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                pushPolygon( a, b, c, 80+sin(fx/10.0f)*50, fx/3.0f, 80-abs(a[3])*3.0f);
                a[0] = 1.0f;
                a[1] = 1.0f;
                a[2] = 0.0f;
                b[0] = 1.0f;
                b[1] = 1.0f;
                b[2] = 0.0f;
                c[0] = -1.0f;
                c[1] = -1.0f;
                c[2] = 1.0f;
                a[0] += sin(fx*12.0123123f) * 12.0f;
                b[0] += sin(fx*12.0123123f) * 12.0f;
                c[0] += sin(fx*12.0123123f) * 12.0f;
                a[1] += cos(fx*33.123123f) * 12.0f;
                b[1] += cos(fx*33.123123f) * 12.0f;
                c[1] += cos(fx*33.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                pushPolygon( a, b, c, 80+sin(fx/10.0f)*50, fx/3.0f, 80-abs(a[3])*3.0f);
            }
            for(fx=0.0f; fx<20.0f; fx++) {
                a[0] = 1.0f;
                a[1] = 1.0f;
                a[2] = 0.0f;
                b[0] = 1.0f;
                b[1] = 1.0f;
                b[2] = 0.0f;
                c[0] = 1.0f;
                c[1] = -1.0f;
                c[2] = 1.0f;
                a[0] += sin(fx*55.0123123f) * 12.0f;
                b[0] += sin(fx*55.0123123f) * 12.0f;
                c[0] += sin(fx*55.0123123f) * 12.0f;
                a[1] += cos(fx*66.123123f) * 12.0f;
                b[1] += cos(fx*66.123123f) * 12.0f;
                c[1] += cos(fx*66.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                pushPolygon( a, b, c, 40+sin(fx/10.0f)*50, fx/3.0f, 120-abs(a[3])*3.0f);
                a[0] = 1.0f;
                a[1] = 1.0f;
                a[2] = 0.01f;
                b[0] = 1.0f;
                b[1] = 1.0f;
                b[2] = 0.01f;
                c[0] = -1.0f;
                c[1] = 1.0f;
                c[2] = 1.0f;
                a[0] += sin(fx*55.0123123f) * 12.0f;
                b[0] += sin(fx*55.0123123f) * 12.0f;
                c[0] += sin(fx*55.0123123f) * 12.0f;
                a[1] += cos(fx*66.123123f) * 12.0f;
                b[1] += cos(fx*66.123123f) * 12.0f;
                c[1] += cos(fx*66.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                pushPolygon( a, b, c, 40+sin(fx/10.0f)*50, fx/3.0f, 120-abs(a[3])*3.0f);
                a[0] = -1.0f;
                a[1] = 1.0f;
                a[2] = 0.01f;
                b[0] = -1.0f;
                b[1] = 1.0f;
                b[2] = 0.01f;
                c[0] = -1.0f;
                c[1] = -1.0f;
                c[2] = 1.0f;
                a[0] += sin(fx*55.0123123f) * 12.0f;
                b[0] += sin(fx*55.0123123f) * 12.0f;
                c[0] += sin(fx*55.0123123f) * 12.0f;
                a[1] += cos(fx*66.123123f) * 12.0f;
                b[1] += cos(fx*66.123123f) * 12.0f;
                c[1] += cos(fx*66.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                pushPolygon( a, b, c, 40+sin(fx/10.0f)*50, fx/3.0f, 120-abs(a[3])*3.0f);
                a[0] = -1.0f;
                a[1] = -1.0f;
                a[2] = 0.01f;
                b[0] = -1.0f;
                b[1] = -1.0f;
                b[2] = 0.01f;
                c[0] = 1.0f;
                c[1] = -1.0f;
                c[2] = 1.0f;
                a[0] += sin(fx*55.0123123f) * 12.0f;
                b[0] += sin(fx*55.0123123f) * 12.0f;
                c[0] += sin(fx*55.0123123f) * 12.0f;
                a[1] += cos(fx*66.123123f) * 12.0f;
                b[1] += cos(fx*66.123123f) * 12.0f;
                c[1] += cos(fx*66.123123f) * 12.0f;
                a[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                b[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                c[2] = fmod(frame*0.05f+fx*4.0f,64.0f)-32.0f;
                pushPolygon( a, b, c, 40+sin(fx/10.0f)*50, fx/3.0f, 120-abs(a[3])*3.0f);
            }
            pos_obj[0] = 0.0f;
            pos_obj[1] = 0.0f;
            pos_obj[2] = 0.0f;
            rot_obj[0] = 0.0f;
            rot_obj[1] = 0.0f;
            rot_obj[2] = 0.0f;
            pos_cam[0] = val_b;
            pos_cam[1] = val_c;
            pos_cam[2] = val_d;
            rot_cam[0] = val_e;
            rot_cam[1] = val_f;
            rot_cam[2] = val_g;
            end3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            rasterize();

            transformer( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            for(fz=0.0f; fz<400.0f; fz++) {
                float fmx = sin(fz*12323.12311231)*7.0f;
                float fmy = cos(fz*72767.5345)*3.0f;
                float fmz = fmod(fz+frame*0.8f,170.0f)-7.0f;
                drawParticle(3.0f*fmx, 3.0f*fmy, fmz, 0x00010000 * (int)(fz / 12.0f) + 0x0100 * (int)abs(sin(fz*0.03f)*13.0f) +  (int)abs(cos(fz*0.12f)*14.0f));
            }
            push_videobuffer();
        
        } else if(((int)val_a)==9){
            float a[3], b[3], c[3];
            unsigned long *isolam32=(unsigned long *)isolam; 
            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] -= videobuffer[i+0] >> 3;
                videobuffer[i+1] -= videobuffer[i+1] >> 2;
                videobuffer[i+2] -= videobuffer[i+2] >> 1;
                videobuffer[i+3] = 0;
            }

            reset_raster();
            setFilled(0);
            setLined(1);
            for(fy = 0.0f; fy < 24.0f; fy++) 
            for(fx = 0.0f; fx < 12.0f; fx++) {
                float a[3], b[3], c[3];
                a[0] = sin(2.0f*3.141592f*fx/12.0f)*12.0f;
                a[1] = cos(2.0f*3.141592f*fx/12.0f)*12.0f;
                a[2] = 0.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                b[0] = sin(2.0f*3.141592f*fx/12.0f)*12.0f;
                b[1] = cos(2.0f*3.141592f*fx/12.0f)*12.0f;
                b[2] = 8.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                c[0] = sin(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                c[1] = cos(2.0f*3.141592f*(fx+1.0f)/12.0f)*12.0f;
                c[2] = 0.0f+(fmod(fy+frame*0.1f,24.0f)-12.0f)*8.0f;
                i += 3;
                pushPolygon( a, b, c, sin(i*0.01421f+frame*0.1f)*32+220, sin(i*0.019421f+frame*0.03f)*127+127, 153);
            } 

            pos_obj[0] = 0.012312f;
            pos_obj[1] = 0.0123123f;
            pos_obj[2] = 0.04564564f;
            rot_obj[0] = 0.01f;
            rot_obj[1] = 0.02f;
            rot_obj[2] = 0.03f;
            pos_cam[0] = sin(frame*0.0455f);
            pos_cam[1] = 0.0f;
            pos_cam[2] = 8.0f;
            rot_cam[0] = 0.0f;
            rot_cam[1] = 1.0f;
            rot_cam[2] = frame*0.03f;
            end3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            rasterize();


            reset_raster();
            setFilled(0);
            setLined(1);
            i=0;
            do {
                float a[3], b[3], c[3];
                int indx_a = model.indecies[i+0] * 3;
                int indx_b = model.indecies[i+1] * 3;
                int indx_c = model.indecies[i+2] * 3;
                a[0] = model.vertices[indx_a+0];
                a[1] = model.vertices[indx_a+1];
                a[2] = model.vertices[indx_a+2];
                b[0] = model.vertices[indx_b+0];
                b[1] = model.vertices[indx_b+1];
                b[2] = model.vertices[indx_b+2];
                c[0] = model.vertices[indx_c+0];
                c[1] = model.vertices[indx_c+1];
                c[2] = model.vertices[indx_c+2];
                i += 3;
                pushPolygon( a, b, c, 153, sin(i*0.01421f+frame*0.1f)*32+220, sin(i*0.019421f+frame*0.03f)*127+127);
            } while(model.index_width/3 > i);


            pos_obj[0] = 0.012312f;
            pos_obj[1] = 0.0123123f;
            pos_obj[2] = 0.04564564f;
            rot_obj[0] = 0.01f;
            rot_obj[1] = 0.02f;
            rot_obj[2] = 0.03f;
            pos_cam[0] = sin(frame*0.0455f);
            pos_cam[1] = 0.0f;
            pos_cam[2] = 8.0f+5.0f*sin(frame*0.01f);
            rot_cam[0] = frame*0.012414f;
            rot_cam[1] = frame*0.024562f;
            rot_cam[2] = frame*0.03f;
            end3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            rasterize();





            
            push_videobuffer();
        }else if(((int)val_a)==10) {
            float a[3], b[3], c[3];
            unsigned long *isolam32=(unsigned long *)isolam; 


            for(i=0; i<320*200*4; i+=4) {
                videobuffer[i+0] = videobuffer[i+0] >> 1;
                videobuffer[i+1] = videobuffer[i+1] >> 1;
                videobuffer[i+2] = videobuffer[i+2] >> 1;
                videobuffer[i+3] = 0;
            }
            i=0;
            for(fy = 0.0f; fy < 25.0f; fy++) {
                ix=0;
                for(fx = 0.0f; fx < 40.0f; fx++) {
                    
                    struct vec d ;
                    struct vec temp;
                    float t = frame*0.1f;
                    float angle, shape, m, n1, n2, n3, rpart, apart, bpart;
                    struct vec symmetry;

                    temp.x = (fx / 40.0f - 0.5f) ;
                    temp.y = (fy / 25.0f - 0.5f) * aspect;

                    d.x = temp.x ;
                    d.y = temp.y ;
                    temp.x += sin(frame*0.03754f)/4.0f;
                    temp.y += cos(frame*0.0212342f)/4.0f;

                    angle = atan2(temp.x, temp.y);

                    d.z = sqrt(temp.x*temp.x+temp.y*temp.y);

                    d.y = frame*0.01f+atan2(temp.x,temp.y)/4.0f;
                    d.x = (1.0f+0.1*sin(angle*(6.0f+sin(frame*0.01f)*6.0f)))*0.2f / d.z ;
                    d.z = 2.0f*(1.0f+sin(angle*(6.0f+sin(frame*0.01f)*6.0f)))+12.0f*max(1.0f-3.0f*sqrt(temp.x*temp.x+temp.y*temp.y),0) ;
                    d.x+= frame*0.01f;
                    uv_tiny[i++] = d;

                    ix++;
                }
                iy++;
            }
            add_stretch_uv( 320, 200);



            reset_raster();
            setFilled(0);
            setLined(1);
            i=0;
            do {
                float a[3], b[3], c[3];
                int indx_a = model.indecies[i+0] * 3;
                int indx_b = model.indecies[i+1] * 3;
                int indx_c = model.indecies[i+2] * 3;
                a[0] = model.vertices[indx_a+0];
                a[1] = model.vertices[indx_a+1];
                a[2] = model.vertices[indx_a+2];
                b[0] = model.vertices[indx_b+0];
                b[1] = model.vertices[indx_b+1];
                b[2] = model.vertices[indx_b+2];
                c[0] = model.vertices[indx_c+0];
                c[1] = model.vertices[indx_c+1];
                c[2] = model.vertices[indx_c+2];
                i += 3;
                pushPolygon( a, b, c, 153, sin(i*0.01421f+frame*0.1f)*32+220, sin(i*0.019421f+frame*0.03f)*127+127);
            } while(model.index_width/3 > i);


            pos_obj[0] = 0.012312f;
            pos_obj[1] = 0.0123123f;
            pos_obj[2] = 0.04564564f;
            rot_obj[0] = 0.01f;
            rot_obj[1] = 0.02f;
            rot_obj[2] = 0.03f;
            pos_cam[0] = sin(frame*0.0455f);
            pos_cam[1] = 0.0f;
            pos_cam[2] = 8.0f+5.0f*sin(frame*0.01f);
            rot_cam[0] = frame*0.012414f;
            rot_cam[1] = frame*0.024562f;
            rot_cam[2] = frame*0.03f;
            end3D( &pos_obj, &rot_obj, &pos_cam, &rot_cam);
            rasterize();





            
            push_videobuffer();
        }
        while (   (inp(INPUT_STATUS) & VRETRACE) ) {};
        while (   !(inp(INPUT_STATUS) & VRETRACE) ) {};

        currentTime = frameCount / 60.0f;
        passedTime = frameCount - prevTime;
        prevTime = currentTime;

        frame = frameCount;

    } 

    //MIDASsetSampleVolume(playHandle, 0);
    end();
    set_mode(TEXT_MODE);

    //printf("Greetings to:\nAdapt, Alumni, Byterapers, Dekadence, Dirty Minds, Epoch, FIT, Flo, Jugz, Jumalauta, LJ, MFX, Mehu, Mercury, Paraguay, Peisik, Prismbeings, Pyrotech, Rtificial, Trilobit\n\n\n\n");

    free(image);
    return 0;
}

