#define VIDEO_INT           0x10      /* the BIOS video interrupt. */
#define SET_MODE            0x00      /* BIOS func to set the video mode. */
#define VGA_256_COLOR_MODE  0x13      /* use to set 256-color mode. */
#define TEXT_MODE           0x03      /* use to set 80x25 text mode. */

#define SC_INDEX            0x03c4    /* VGA sequence controller */
#define SC_DATA             0x03c5
#define PALETTE_INDEX       0x03c8    /* VGA digital-to-analog converter */
#define PALETTE_DATA        0x03c9
#define GC_INDEX            0x03ce    /* VGA graphics controller */
#define GC_DATA             0x03cf
#define CRTC_INDEX          0x03d4    /* VGA CRT controller */
#define CRTC_DATA           0x03d5
#define INPUT_STATUS        0x03da

#define MAP_MASK            0x02      /* Sequence controller registers */
#define ALL_PLANES          0xff02
#define MEMORY_MODE         0x04

#define LATCHES_ON          0x0008    /* Graphics controller registers */
#define LATCHES_OFF         0xff08

#define HIGH_ADDRESS        0x0C      /* CRT controller registers */
#define LOW_ADDRESS         0x0D
#define UNDERLINE_LOCATION  0x14
#define MODE_CONTROL        0x17

#define DISPLAY_ENABLE      0x01      /* VGA input status bits */
#define VRETRACE            0x08

#define SCREEN_WIDTH        320       /* width in pixels of mode 0x13 */
#define SCREEN_HEIGHT       200       /* height in pixels of mode 0x13 */
#define SCREEN_SIZE         (word)(SCREEN_WIDTH*SCREEN_HEIGHT)
#define NUM_COLORS          256       /* number of colors in mode 0x13 */

#define BITMAP_WIDTH        32
#define BITMAP_HEIGHT       25
#define ANIMATION_FRAMES    24
#define TOTAL_FRAMES        140
#define VERTICAL_RETRACE              /* comment out this line for more accurate timing */

#define PALETTE_INDEX       0x03c8
#define PALETTE_DATA        0x03c9
#define INPUT_STATUS        0x03da
#define VRETRACE            0x08

#define NEW(c) (malloc((c)))
/* CRT controller registers */
#define HIGH_ADDRESS 0x0C
#define LOW_ADDRESS  0x0D

typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned long  dword;

/* Display refresh rate */
static unsigned refreshRate; 
unsigned int    visible_page        =   0;
unsigned int    non_visible_page    =   320*200/4;
word            temp;

static int      border = 0;
word *my_clock=(word *)0x0000046C;    /* this points to the 18.2hz system*/

#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <conio.h>
#include <mem.h>
#include <math.h>
#include <float.h>

byte chunkymode = 1;
byte chainmode = 0;
byte extrememode = 0;
word            temp;
volatile long   frameCount = 0;
#define FIXED_TO_FLOAT(x) ((float)(x >> 16));
#define FLOAT_TO_FIXED(f) (long)(f * 65536);
#define INT_TO_FIXED(x) (long)(x << 16)
#define FIXED_TO_INT(x) (int) ( (long)(x >> 16) & 0x0000ffff)
#define fastsin(x) pre_sin[((int)abs((float)x*1024.0f))&1023]

/*  long long -> turns into 64-bit
    Any 32-bit CPU or even MMX will go ape shit over this
    and lose any speed gain from fixed*/
//#define FIXEDMUL(x, y) ((long long)(x)*(long long)(y)) / 65536

/*  Not super accurate but good enough*/
#define FIXEDMUL(x, y) ((x>>8)*(y>>8))

//#define FIXEDDIV(x, y) (((long long)(x)<<16)/y)
#define FIXEDDIV(x, y) ((x<<5)/y)<<11 

#define RGB888 1
#define RGBX8888 2
#define FAKETRUE 0
byte videotype = FAKETRUE;

long beat;