#include "src\model\model.h"
struct Model loadModel(char *vertice_filename, char *indecies_filename) {
	struct Model model;
	FILE *vertice_file;
	FILE *face_file;
    float x[3];
	int i = 0;
	int j = 0;
	int size = 0;
	printf("reading vertex file : %s\n", vertice_filename);
	vertice_file = fopen ( vertice_filename, "rb") ;

	fread( NULL, sizeof(int), 1, vertice_file);
 
	while(!feof(vertice_file)) {
		fread( &(model.vertices[i++]), sizeof(float), 1, vertice_file);
		size++;
		if((size & 1023) == 0) {
			printf("         %i\n",size);
		}
		printf("%f\n",model.vertices[i-1]);
	} 
    fclose(vertice_file);
	printf("%i\n",size);
	i = 0;
	size = 1;
	face_file = fopen ( indecies_filename, "rb") ;
	printf("reading faces file : %s\n", indecies_filename);
	fread( &(model.index_width), sizeof(int), 1, face_file);
	while(!feof(face_file)) {

		fread( &(model.indecies[i]), sizeof(int), 1, face_file);
		i++;
		fread( &(model.indecies[i]), sizeof(int), 1, face_file);
		i++;
		fread( &(model.indecies[i]), sizeof(int), 1, face_file);
		i++;



		fread( &(model.normals[j]), sizeof(float), 1, face_file);
		j++;
		fread( &(model.normals[j]), sizeof(float), 1, face_file);
		j++;
		fread( &(model.normals[j]), sizeof(float), 1, face_file);
		j++;
		if(i>800) break;

	} 
	model.index_width = i;
	printf("model index width: %i \n",model.index_width);
	fclose(face_file);
    return model;
}
struct Mass loadMass(char *vertice_filename) {
	struct Mass model;
	FILE *vertice_file;
    float x[3];
	int i = 0;
	int j = 0;
	int size = 0;
	printf("reading vertex file : %s\n", vertice_filename);
	vertice_file = fopen ( vertice_filename, "rb") ;

	fread( NULL, sizeof(int), 1, vertice_file);
 
	i = 0;
	j = 0;
	while(!feof(vertice_file)) {
		fread( &(model.vertices[i++]), sizeof(float), 1, vertice_file);
		size++;
		fread( &(model.vertices[i++]), sizeof(float), 1, vertice_file);
		size++;
		fread( &(model.vertices[i++]), sizeof(float), 1, vertice_file);
		size++;
		fread( &(model.color[j++]), sizeof(float), 1, vertice_file);
		size++;
		fread( &(model.color[j++]), sizeof(float), 1, vertice_file);
		size++;
		fread( &(model.color[j++]), sizeof(float), 1, vertice_file);
		size++;
		i+=3;
		if(i>2000) break;
	} 
	model.index_width = i;
    fclose(vertice_file);
	printf("%i\n",size);
    return model;
}