#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
struct Model {
	float vertices[800];
	float normals[800];
	int indecies[800];	
	int index_width;
};
struct Model loadModel(char *vertice_filename, char *indecies_filename);
struct Mass {
	float vertices[6000];
	float color[6000];
	int index_width;
};
struct Mass loadMass(char *vertice_filename);