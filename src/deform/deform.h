void set_uv(unsigned long lx, unsigned long ly, float fx, float fy);
void interpolate_onto_screen(unsigned long *texture, float width, float height); /*  non-exact uv coordinate interpolator */
void stretch_uv(float width, float height) ;
struct vec vec2( float _x, float _y);
void gen_texture(unsigned char *texture);
void add_stretch_uv(float width, float height);