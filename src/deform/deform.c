#include "src\misc\muual.h"

struct vec {
	float x;
	float y;
	float z;
};
struct vec vec2( float _x, float _y) {
	struct vec a;
	a.x = _x;
	a.y = _y;
	return a;
}
struct vec vec3( float _x, float _y, float _z) {
	struct vec a;
	a.x = _x;
	a.y = _y;
	a.z = _z;
	return a;
}
unsigned char uvtexture[16 /*shades*/ * 320 * 200 * 4 ];
dword *uvtexture32=(dword *)&uvtexture; 
dword gradient[40*25];
struct vec uv_tiny[40*25];
struct vec uv_short[320*25];

struct vec uv_coords[80*50];
struct vec uv_coords2x[160*100];
struct vec uv_coords4x[320*200];
extern byte videobuffer[320*400*3];
extern dword *videobuffer32=(dword *)&videobuffer; 
void gen_texture(unsigned char *texture) {
	int i, j, k;
	int indx=0;
	for(k=0; k<16; k++) {
		indx = 0;
		for(j=0; j<200; j++) {
			for(i=0; i<320; i++) {
				uvtexture[indx+k*320*200*4] = texture[indx] / (1.4f+k*0.3f+k*k*0.13f);
				indx++;
				uvtexture[indx+k*320*200*4] = texture[indx] / (1.4f+k*0.3f+k*k*0.13f);
				indx++;
				uvtexture[indx+k*320*200*4] = texture[indx] / (1.4f+k*0.3f+k*k*0.13f);
				indx++;
				uvtexture[indx+k*320*200*4] = texture[indx] / (1.4f+k*0.3f+k*k*0.13f);
				indx++;
			}
		}
	}
}
void stretch_uv(float width, float height) { /*  non-exact uv coordinate interpolator */
	int _x, _y;
	int _yp1; //y+1
	int bx, by;
	int iwidth = width;
	int iheight = height;
	by = 0;
	for(_y = 2; _y < 25; _y++) {
		int by320 = by*320;
		bx = 0;
		for(_x = 0; _x < 40; _x++) {
			int pos_left = _x + _y * 40;
			int pos_right = pos_left+1;
			int bxy;
			float left_val_x = uv_tiny[pos_left].x;
			float left_val_y = uv_tiny[pos_left].y;
			float right_val_x = uv_tiny[pos_right].x;
			float right_val_y = uv_tiny[pos_right].y;
			bx = _x*8;
			bxy = bx+by320;
			uv_short[bxy] = uv_tiny[pos_left];
			bxy+=1;
			uv_short[bxy].x = left_val_x * (6.0f / 7.0f) + right_val_x * (1.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (6.0f / 7.0f) + right_val_y * (1.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (5.0f / 7.0f) + right_val_x * (2.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (5.0f / 7.0f) + right_val_y * (2.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (4.0f / 7.0f) + right_val_x * (3.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (4.0f / 7.0f) + right_val_y * (3.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (3.0f / 7.0f) + right_val_x * (4.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (3.0f / 7.0f) + right_val_y * (4.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (2.0f / 7.0f) + right_val_x * (5.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (2.0f / 7.0f) + right_val_y * (5.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x / 7.0f + right_val_x * (6.0f / 7.0f);
			uv_short[bxy].y = left_val_y / 7.0f + right_val_y * (6.0f / 7.0f);
			bxy+=1;
			uv_short[bxy] = uv_tiny[pos_right];
		}
		by++;
	}


	
	by = 0;
	for(_y = 3; _y < 25-3; _y++) {
		int y320 = _y*320;
		int y320p1 = (_y+1)*320;
		_yp1 = _y + 1;
		for(_x = 8; _x < 320-8; _x++) {
			struct vec va,vb,vc,vd,ve,vf,vg,vh;
			unsigned int ix, iy;
			int by320 = _y*8*320;
			float uvshortx = uv_short[_x+y320].x;
			float uvshorty = uv_short[_x+y320].y;
			float uvshortx2 = uv_short[_x+y320p1].x;
			float uvshorty2 = uv_short[_x+y320p1].y;
			int shade = 320 * 200 * (int)uv_tiny[_x/8+_y*40+((_x+0)&1)].z;
			va = uv_short[_x+y320];
			vb.x = uvshortx * (6.0f / 7.0f) + uvshortx2 * ( 1.0f / 7.0f);
			vb.y = uvshorty * (6.0f / 7.0f) + uvshorty2 * ( 1.0f / 7.0f);
			vc.x = uvshortx * (5.0f / 7.0f) + uvshortx2 * ( 2.0f / 7.0f);
			//vc.y = uvshorty * (5.0f / 7.0f) + uvshorty2 * ( 2.0f / 7.0f);
			vc.y = vb.y;
			//vd.x = uvshortx * (4.0f / 7.0f) + uvshortx2 * ( 3.0f / 7.0f);
			vd.x = vc.x;
			vd.y = uvshorty * (4.0f / 7.0f) + uvshorty2 * ( 3.0f / 7.0f);
			ve.x = uvshortx * (3.0f / 7.0f) + uvshortx2 * ( 4.0f / 7.0f);
			ve.y = uvshorty * (3.0f / 7.0f) + uvshorty2 * ( 4.0f / 7.0f);
			vf.x = uvshortx * (2.0f / 7.0f) + uvshortx2 * ( 5.0f / 7.0f);
			vf.y = uvshorty * (2.0f / 7.0f) + uvshorty2 * ( 5.0f / 7.0f);
			vg.x = uvshortx * (1.0f / 7.0f) + uvshortx2 * ( 6.0f / 7.0f);
			//vg.y = uvshorty * (1.0f / 7.0f) + uvshorty2 * ( 6.0f / 7.0f);
			vg.y = vf.y;
			vh = uv_short[_x+y320p1];
			ix = (int)( va.x * width);
			iy = (int)( va.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vb.x * width);
			iy = (int)( vb.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vc.x * width);
			iy = (int)( vc.y * height);
			ix = ix & 127;
			iy = iy & 127; 
			iy *= iwidth;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vd.x * width);
			iy = (int)( vd.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( ve.x * width);
			iy = (int)( ve.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			shade = 320 * 200 * (int)uv_tiny[_x/8+_y*40+((_x+1)&1)].z;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vf.x * width);
			iy = (int)( vf.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vg.x * width);
			iy = (int)( vg.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vh.x * width);
			iy = (int)( vh.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] = uvtexture32[ix + iy + shade];

		}
	}
	

}
int f=0;
void add_stretch_uv(float width, float height) { /*  non-exact uv coordinate interpolator */
	int _x, _y;
	int _yp1; //y+1
	int bx, by;
	int iwidth = width;
	int iheight = height;
	by = 0;
	f++;
	for(_y = 2; _y < 25; _y++) {
		int by320 = by*320;
		bx = 0;
		for(_x = 0; _x < 40; _x++) {
			int pos_left = _x + _y * 40;
			int pos_right = pos_left+1;
			int bxy;
			float left_val_x = uv_tiny[pos_left].x;
			float left_val_y = uv_tiny[pos_left].y;
			float right_val_x = uv_tiny[pos_right].x;
			float right_val_y = uv_tiny[pos_right].y;
			bx = _x*8;
			bxy = bx+by320;
			uv_short[bxy] = uv_tiny[pos_left];
			bxy+=1;
			uv_short[bxy].x = left_val_x * (6.0f / 7.0f) + right_val_x * (1.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (6.0f / 7.0f) + right_val_y * (1.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (5.0f / 7.0f) + right_val_x * (2.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (5.0f / 7.0f) + right_val_y * (2.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (4.0f / 7.0f) + right_val_x * (3.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (4.0f / 7.0f) + right_val_y * (3.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (3.0f / 7.0f) + right_val_x * (4.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (3.0f / 7.0f) + right_val_y * (4.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (2.0f / 7.0f) + right_val_x * (5.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (2.0f / 7.0f) + right_val_y * (5.0f / 7.0f);
			bxy+=1;
			uv_short[bxy].x = left_val_x * (1.0f / 7.0f) + right_val_x * (6.0f / 7.0f);
			uv_short[bxy].y = left_val_y * (1.0f / 7.0f) + right_val_y * (6.0f / 7.0f);
			bxy+=1;
			uv_short[bxy] = uv_tiny[pos_right];
		}
		by++;
	}


	
	by = 0;
	for(_y = 3; _y < 25-3; _y++) {
		int y320 = _y*320;
		int y320p1 = (_y+1)*320;
		_yp1 = _y + 1;
		for(_x = 16; _x < 320-16; _x++) {
			struct vec va,vb,vc,vd,ve,vf,vg,vh;
			unsigned int ix, iy;
			int by320 = _y*8*320;
			float uvshortx = uv_short[_x+y320].x;
			float uvshorty = uv_short[_x+y320].y;
			float uvshortx2 = uv_short[_x+y320p1].x;
			float uvshorty2 = uv_short[_x+y320p1].y;
			int shade = 320 * 200 * (int)uv_tiny[_x/8+(_y+3)*40+((_x+0+f*2)&3)].z;
			va = uv_short[_x+y320];
			vb.x = uvshortx * (6.0f / 7.0f) + uvshortx2 * ( 1.0f / 7.0f);
			vb.y = uvshorty * (6.0f / 7.0f) + uvshorty2 * ( 1.0f / 7.0f);
			vc.x = uvshortx * (5.0f / 7.0f) + uvshortx2 * ( 2.0f / 7.0f);
			//vc.y = uvshorty * (5.0f / 7.0f) + uvshorty2 * ( 2.0f / 7.0f);
			vc.y = vb.y;
			//vd.x = uvshortx * (4.0f / 7.0f) + uvshortx2 * ( 3.0f / 7.0f);
			vd.x = vc.x;
			vd.y = uvshorty * (4.0f / 7.0f) + uvshorty2 * ( 3.0f / 7.0f);
			ve.x = uvshortx * (3.0f / 7.0f) + uvshortx2 * ( 4.0f / 7.0f);
			//ve.y = uvshorty * (3.0f / 7.0f) + uvshorty2 * ( 4.0f / 7.0f);
			ve.y = vd.y;
			vf.x = uvshortx * (2.0f / 7.0f) + uvshortx2 * ( 5.0f / 7.0f);
			vf.y = uvshorty * (2.0f / 7.0f) + uvshorty2 * ( 5.0f / 7.0f);
			vg.x = uvshortx * (1.0f / 7.0f) + uvshortx2 * ( 6.0f / 7.0f);
			//vg.y = uvshorty * (1.0f / 7.0f) + uvshorty2 * ( 6.0f / 7.0f);
			vg.y = vf.y;
			vh = uv_short[_x+y320p1];
			ix = (int)( va.x * width);
			iy = (int)( va.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vb.x * width);
			iy = (int)( vb.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vc.x * width);
			iy = (int)( vc.y * height);
			ix = ix & 127;
			iy = iy & 127; 
			iy *= iwidth;
			shade = 320 * 200 * (int)uv_tiny[_x/8+(_y+3)*40+((_x-1+f*2)&3)].z;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vd.x * width);
			iy = (int)( vd.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( ve.x * width);
			iy = (int)( ve.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			shade = 320 * 200 * (int)uv_tiny[_x/8+(_y+3)*40+((_x+1+f*2)&3)].z;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vf.x * width);
			iy = (int)( vf.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vg.x * width);
			iy = (int)( vg.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];
			by320+=320;
			ix = (int)( vh.x * width);
			iy = (int)( vh.y * height);
			ix = ix & 127;
			iy = iy & 127;
			iy *= iwidth;
			videobuffer32[_x+by320] += uvtexture32[ix + iy + shade];

		}
	}
	

}